/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PatientFunctions;

import MedicinePackage.Prescription;
import RatingsPackage.RatingsCollection;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author jedwards11
 */
public interface PatientStatus {
    
    public abstract void requestAccountCreation();
    
    public abstract void giveFeedback(String doctorID, int rating, String feedback);
    
    public abstract RatingsCollection viewDoctorRating(String doctorID);
    
    public abstract List<String> viewHistory();
    
    public abstract void requestAppointment(Timestamp apptmtTime, String DoctorID);
    
    public abstract void requestPrescription(Prescription prescription);
    
    public abstract List<Prescription> viewPrescriptions();
    
    public abstract void requestTermination();
    
    
}
