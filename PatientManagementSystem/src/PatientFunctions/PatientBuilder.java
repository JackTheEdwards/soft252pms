/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PatientFunctions;

import UserPackage.PatientDetails;
import HospitalAdmin.DataAccess;
import LoginPackage.LoginDetails;
import UserPackage.Sex;
import java.sql.Timestamp;

/**
 *
 * @author jedwards11
 */
public class PatientBuilder {
    
    private Sex sex;
    private Timestamp dateOfBirth;
    private LoginDetails login;
    private String givenName;
    private String surname;
    private String address;
    
    /**
     *
     * @return
     */
    public static PatientBuilder newInstance(){
        return new PatientBuilder();
    }
    
    private PatientBuilder(){
        
    }
    
    /** setUserName()
     * 
     * sets username for builder
     *
     * @return this builder object with newly set fields
     */
    public PatientBuilder setUserName(){
        String IDNumber = String.format("%04d", DataAccess.getData().getUsers().size());
        
        String patientID = "P" + IDNumber;
        
        this.login.setUserName(patientID);
        
        return this;
    }

    /**setPassword()
     *
     * sets password for builder
     * 
     * @param password the password to be set
     * @return this builder object
     */
    public PatientBuilder setPassword(String password){
        this.login.setPassWord(password);
        return this;
    }
    
    /**setLogin()
     * 
     * sets LoginDetails for builder
     *
     * @param password the password to be set
     * @return this builder object with newly set fields
     */
    public PatientBuilder setLogin(String password){
        
        String IDNumber = String.format("%04d", DataAccess.getData().getUsers().size());
        
        String patientID = "P" + IDNumber;
        
        LoginDetails login = new LoginDetails(patientID, password);
        
        this.login = login;
        
        return this;
    }
    
    /**setGivenName()
     *
     * sets givenName for builder
     * 
     * @param givenName
     * @return this builder object with newly set fields
     */
    public PatientBuilder setGivenName(String givenName){
        this.givenName = givenName;
        
        return this;
    }

    /**setSurname()
     *
     * sets surname for builder
     *  
     * @param surname surname to set
     * @return this builder object with newly set fields
     */
    public PatientBuilder setSurname(String surname){
        this.surname = surname;
        
        return this;
    }

    /**setSex()
     * 
     * sets sex for builder
     * 
     * @param sex sex to set
     * @return this builder object with newly set fields
     */
    public PatientBuilder setSex(Sex sex){
        this.sex = sex;
        
        return this;
    }

    /**setAddress()
     * 
     *sets address for builder
     * 
     * @param address address to set
     * @return this builder object with newly set fields
     */
    public PatientBuilder setAddress(String address){
        this.address = address;
        
        return this;
    }

    /**setDateOfBirth()
     *
     * sets DOB for builder
     * 
     * @param DOB
     * @return this builder object with newly set fields
     */
    public PatientBuilder setDateOfBirth(Timestamp DOB){
        this.dateOfBirth = DOB;
        
        return this;
    }

    /**build()
     * 
     * creates a new PatienDetails object with the builder specified field 
     *
     * @return PatientDetails the patientDetails to return
     */
    public PatientDetails build(){
        return new PatientDetails (this);
    }

    /**getSex()
     *
     * accessor method for sex field
     * 
     * @return sex field
     */
    public Sex getSex() {
        return sex;
    }

    /**getDateOfBirth()
     *
     * accessor method for dateOfBirth
     * 
     * @return dateOfBirth field
     */
    public Timestamp getDateOfBirth() {
        return dateOfBirth;
    }

    /**getLogin()
     *
     * accessor method for login
     * 
     * @return login
     */
    public LoginDetails getLogin() {
        return login;
    }

    /**getGivenName()
     *
     * accessor method for givenName
     * 
     * @return the given name
     */
    public String getGivenName() {
        return givenName;
    }

    /**getSurname()
     *
     * accessor method for surname
     * 
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**getAddress()
     *
     * accessor method for address field
     * 
     * @return address
     */
    public String getAddress() {
        return address;
    }
    
    
    
}
