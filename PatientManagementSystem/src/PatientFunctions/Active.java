/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PatientFunctions;

import AppointmentPackage.Appointment;
import HospitalAdmin.DataAccess;
import MedicinePackage.Prescription;
import NotificationsPackage.Request;
import NotificationsPackage.RequestAppointment;
import NotificationsPackage.RequestCreateAccount;
import NotificationsPackage.RequestMedicine;
import NotificationsPackage.RequestTerminateAccount;
import RatingsPackage.RatingsCollection;
import RatingsPackage.Review;
import UserPackage.PatientDetails;
import java.util.List;
import java.sql.Timestamp;

/**
 *
 * @author jedwards11
 */
public class Active implements PatientStatus{

    /**requestAccountCreation()
     *
     * sends a request to approve the current account
     */
    @Override
    public void requestAccountCreation() {
        
        String requestString = "User : " +
                DataAccess.getData().getCurrentUser().getLogin().getUserName()+
                " " +
                DataAccess.getData().getCurrentUser().getGivenName()+
                " " + 
                DataAccess.getData().getCurrentUser().getSurname()+
                " is requesting their account be activated"
                ;
        
        Request newAccountActivation = new RequestCreateAccount(DataAccess.getData().getCurrentUser(), requestString);
        
        DataAccess.getData().getSecretaryRequests().add(newAccountActivation);
        DataAccess.getData().saveData();
    }

    /**giveFeedback(String doctorID, int rating, String feedback)
     *
     * creates a review with the given rating and feedback, and returns it
     * to a doctor specified by the parameter
     * 
     * @param doctorID the ID of the doctor being reviewed
     * @param rating the rating of the doctor being reviewed
     * @param feedback the comments of the review of the doctor provided by current patient
     */
    @Override
    public void giveFeedback(String doctorID, int rating, String feedback) {
        
        Review reviewToAdd = new Review(
                feedback,
                DataAccess.getData().getCurrentUser().getLogin().getUserName(),
                rating
        );
        reviewToAdd.toString();
        
        DataAccess.getData().findDoctor(doctorID).getReviews().addReview(reviewToAdd);
        DataAccess.getData().saveData();
    }

    /**viewDoctorRating(String doctorID)
     * 
     * returns the RatingsCollection of the doctor specified by the ID
     *
     * @param doctorID the ID of the doctor who's reviews are being fetched
     * @return the ratings collection of the doctor to be returned
     */
    @Override
    public RatingsCollection viewDoctorRating(String doctorID) {
        
        RatingsCollection reviewsToReturn;
        
        reviewsToReturn = DataAccess.getData().findDoctor(doctorID).getReviews();
        
        return reviewsToReturn;
    }

    /** viewHistory()
     *
     * returns the history of the current patient
     * 
     * @return the History of the current patient
     */
    @Override
    public List<String> viewHistory() {
        
        PatientDetails patient = (PatientDetails)DataAccess.getData().getCurrentUser();
        
        return patient.getPatientNotes();
        
    }

    /**requestAppointment()
     *
     * requests an account with a specified doctor
     * 
     * @param apptmtTime the time of the appointment requested
     * @param DoctorID the ID of the doctor to be requested
     */
    @Override
    public void requestAppointment(Timestamp apptmtTime, String DoctorID) {
        
        Appointment proposedAppointment = new Appointment(DataAccess.getData().getCurrentUser().getLogin().getUserName(), apptmtTime);
        
        Request apptmtRequest = new RequestAppointment(DataAccess.getData().getCurrentUser(),
        "Appointment Request from "+
                DataAccess.getData().getCurrentUser().getLogin().getUserName()+
                " for "+
                DoctorID+
                " at "+
                apptmtTime,
                proposedAppointment,
                DoctorID
        );
        
        DataAccess.getData().getSecretaryRequests().add(apptmtRequest);
        DataAccess.getData().saveData();
    }

    /**viewPrescriptions()
     *
     * returns the prescriptions of the current patient
     * 
     * @return the prescriptions to be returned
     */
    @Override
    public List<Prescription> viewPrescriptions() {
        
        PatientDetails patient = (PatientDetails)DataAccess.getData().getCurrentUser();
        
        return patient.getPrescriptions();
        
    }

    /**requestTermination()
     *
     * requests that the account be terminated
     */
    @Override
    public void requestTermination() {
        
        RequestTerminateAccount newTerminateRequest = new RequestTerminateAccount(DataAccess.getData().getCurrentUser(), "Request To Delete Account : " + DataAccess.getData().getCurrentUser().getLogin().getUserName());
        
        DataAccess.getData().getSecretaryRequests().add(newTerminateRequest);
        DataAccess.getData().saveData();
    }

    /**requestPrescription()
     *
     * sends a request for a prescription
     * 
     * @param prescription the prescription requested
     */
    @Override
    public void requestPrescription(Prescription prescription) {
        
        String message = 
                "Request from  " 
                + DataAccess.getData().
                getCurrentUser().
                getLogin().
                getUserName() 
                + " to pick up their prescription of" 
                + prescription.getMeds().getName();
        
        RequestMedicine newPrescription = new RequestMedicine(DataAccess.getData().getCurrentUser() ,message, prescription);
        
        DataAccess.getData().getSecretaryRequests().add(newPrescription);
        
        
    }
    
    
    
}
