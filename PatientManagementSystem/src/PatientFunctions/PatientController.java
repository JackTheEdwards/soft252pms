/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PatientFunctions;

import HospitalAdmin.Controller;
import HospitalAdmin.ControllerStrategy;
import HospitalAdmin.DataAccess;
import HospitalAdmin.InitialController;
import MedicinePackage.Prescription;
import NotificationsPackage.RequestMedicine;
import RatingsPackage.RatingsCollection;
import RatingsPackage.Review;
import UserPackage.DoctorDetails;
import UserPackage.PatientDetails;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class PatientController extends ControllerStrategy{
    
    int currentMessage;
    
    PatientFunc tools;
    PatientForm view;
    PatientDoctorsForm viewDoctor;
    
    LogoutListener logoutListener;
    
    ViewDoctorPageListener viewDoctorPageListener ;
    RequestTerminateListener requestTerminationListener;
    NextMessageListener nextMsgListener;
    DeleteMessageListener deleteMsgListener;
    RequestPrescriptionListener requestMedsListener;
    
    
    RequestAppointmentListener requestAppointmentListener;
    SendReviewListener sendReviewListener;
    ViewPatientListener viewPatientListener;
    ChangeDoctorListener changeDoctorListener;
    
    /** PatientController()
     * 
     * constructor for PatientController class,
     *
     * @param controller the super controller
     */
    public PatientController(Controller controller) {
        
        super(controller);
        
        //Initializes the forms
        view = new PatientForm();
        viewDoctor = new PatientDoctorsForm();
        
        //initializes generic logoutListener
        logoutListener = new LogoutListener();
        
        //initializes relevant listeners for standard patient notes page
        viewDoctorPageListener = new ViewDoctorPageListener();
        requestTerminationListener = new RequestTerminateListener() ;
        nextMsgListener = new NextMessageListener();
        deleteMsgListener = new DeleteMessageListener();
        requestMedsListener = new RequestPrescriptionListener();
        
        // initializes relevant listeners for viewDoctorsPage
        changeDoctorListener = new ChangeDoctorListener();
        viewPatientListener = new ViewPatientListener ();
        requestAppointmentListener = new RequestAppointmentListener();
        sendReviewListener = new SendReviewListener();
        
        //initializes tools for patient functionality
        tools = new PatientFunc();
        
        PatientDetails currentUser = (PatientDetails)DataAccess.getData().getCurrentUser();
        //setss the status to 
        tools.setStatus(currentUser.getStatus());
        setViewStatus(currentUser.getStatus());
        
        //add generic logout listeners
        view.addLogoutListener(logoutListener);
        viewDoctor.addLogoutListener(logoutListener);
        
        //add Listeners for generic view
        view.addRequestTerminationListener(requestTerminationListener);
        view.addViewDoctorPageListener(viewDoctorPageListener);
        view.addBtnDeleteMessageListener(deleteMsgListener);
        view.addBtnNextMessageListener(nextMsgListener);
        view.addBtnRequestPrescription(requestMedsListener);
        
        //add listeners for viewDoctorForm
        viewDoctor.addChangeDoctorListener(changeDoctorListener);
        viewDoctor.addRequestAppointmentListener(requestAppointmentListener);
        viewDoctor.addViewPatientListener(viewPatientListener);
        viewDoctor.addSendReviewListener(sendReviewListener);
        
        //gets doctors into doctor list
        viewDoctor.setDoctors(DataAccess.getData().getUserGroup("Doctor"));
        
        //sets Notification field
        setNotification(currentMessage);
        
        //sets relevant fields in view
        setPatientDetails();
        setPresciptions();
        
        view.setVisible(true);
        
    }
    
    
    private void setReviews(String doctorID){
        
        List<String> reviews = new ArrayList<String>();
        DoctorDetails currentDoctor = (DoctorDetails)DataAccess.getData().findDoctor(doctorID);
        RatingsCollection reviewCollection = currentDoctor.getReviews();
        
        viewDoctor.setReviews(reviews);
        
        if (!DataAccess.getData().findDoctor(doctorID).getReviews().getReviews().isEmpty()){
            for (Review currentReview: DataAccess.getData().findDoctor(doctorID).getReviews().getReviews()){
                reviews.add("Rating : " + currentReview.getRating()+ " \"" + currentReview.getFeedback() + "\" by user: " + currentReview.getAuthor());
            }
            DataAccess.getData().findDoctor(doctorID).getReviews().getAverageRating();
            viewDoctor.setRating(DataAccess.getData().findDoctor(doctorID).getReviews().getAverageRating());
            viewDoctor.setReviews(reviews);
        }
        
        
    }
    
    private void setPresciptions (){
        
        List<String> prescriptionNames = new ArrayList<String>();
        
        List<Prescription> prescriptions = DataAccess.getData().findPatient(DataAccess.getData().getCurrentUser().getLogin().getUserName()).getPrescriptions();
        
            for (Prescription prescription: prescriptions){
                
                prescriptionNames.add(prescription.getMeds().getName());
                
            }
        
            view.setComboBoxPrescriptions(prescriptionNames);
        
    }
    
    private void setPatientDetails (){
        PatientDetails currentUser = (PatientDetails) DataAccess.getData().getCurrentUser();
            
            List<String> prescriptionList = new ArrayList<String>();
            
            for (Prescription currentPrescription:currentUser.getPrescriptions()){
                prescriptionList.add("Prescription for "+
                        currentPrescription.getMeds().getName() + 
                        " of " +
                        currentPrescription.getMeds().getQuantity()+
                        " , "+
                        currentPrescription.getInstructions() );
            }
            
            view.setMyHistory(currentUser.getPatientNotes());
            view.setMyPrescriptions(prescriptionList);
    }
    
    private void setViewStatus (boolean status){
        
        view.enableDoctorPageButton(status);
        view.enableRequestTerminationButton(status);
        
        if (status == false){
            
            JOptionPane.showMessageDialog(new JFrame(),"Account is not enabled, please contact one of our Administrative Staff");
            
        }
        
    }
    
    private void setNotification (int notificationNumber){
        
        if (DataAccess.getData().getCurrentUser().getReceivedNotifications().isEmpty()){
                view.setTxtMessageHolder("");
            } else{
                view.setTxtMessageHolder(
                DataAccess.
                getData().
                        findPatient(
                                DataAccess.
                                        getData().
                                        getCurrentUser().
                                        getLogin().
                                        getUserName()
                        ).
                        getReceivedNotifications().
                        get(currentMessage).
                        getMessage());
        
            }
        
        
        
        
    }
    

    class ViewDoctorPageListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.setVisible(false);
            
            viewDoctor.setVisible(true);
        }
        
    }
    
  
    
    
    class LogoutListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            view.setVisible(false);
            viewDoctor.setVisible(false);
            
            view.dispose();
            viewDoctor.dispose();
            
            getController().setStrategy(new InitialController(getController()));
            
        }
        
    }
    
    class RequestTerminateListener extends LogoutListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            tools.setStatus(false);
            
            
            
            super.actionPerformed(e);
            //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
    class RequestAppointmentListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            tools.requestAppointment(viewDoctor.getAppointmentTime(), viewDoctor.getDoctorID());
        
        }
        
    }
    
    
    
    class SendReviewListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            tools.giveFeedback(viewDoctor.getDoctorID(),
                    viewDoctor.getRating()
                    ,  viewDoctor.getReview());
            
            setReviews(viewDoctor.getDoctorID());
        }
        
    }
    
    class ChangeDoctorListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            viewDoctor.setDoctors(DataAccess.getData().getUserGroup("Doctor"));
            setReviews(viewDoctor.getDoctorID());
            
        }

        
    }
    
    class ViewPatientListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            view.setVisible(true);
            
            viewDoctor.setVisible(false);
            
        }
        
    }
    
    class NextMessageListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if (currentMessage < DataAccess.getData().getCurrentUser().getReceivedNotifications().size()-1){
                currentMessage++;
            }
            
            setNotification(currentMessage);
            
        }
        
    }
    
    class DeleteMessageListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if (DataAccess.getData().getCurrentUser().getReceivedNotifications().isEmpty() == false){
                
                DataAccess.getData().getCurrentUser().getReceivedNotifications().remove(currentMessage);
                
            }
            
            if (currentMessage >= DataAccess.getData().getCurrentUser().getReceivedNotifications().size()){
                
                currentMessage = DataAccess.getData().getCurrentUser().getReceivedNotifications().size()-1;
            
            }
            
            setNotification(currentMessage);
            
        }
        
    }
    
    class RequestPrescriptionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            System.out.println("listener fire");
            
            List <Prescription> prescriptions = DataAccess.
                    getData().
                    findPatient(
                            DataAccess.
                                    getData().
                                    getCurrentUser().
                                    getLogin().
                                    getUserName()
                    ).getPrescriptions();
            
            
            if (view.getComboBoxPrescriptionsContent() != null){
                
                System.out.println("condition fire");
                
                Prescription prescriptionToRequest = null;
            
                for (Prescription prescriptionToCheck: prescriptions){
                    
                    if (prescriptionToCheck.getMeds().getName().equals(view.getComboBoxPrescriptionsContent())){
                        
                        prescriptionToRequest = prescriptionToCheck;
                        
                        System.out.println(prescriptionToRequest);
                    
                    }
                }
                
                
                RequestMedicine requestMeds = new RequestMedicine(
                        DataAccess.getData().getCurrentUser(),
                        DataAccess.
                                getData().
                                getCurrentUser().
                                getLogin().
                                getUserName()
                                + 
                                " requests their prescription of "
                                +
                                view.getComboBoxPrescriptionsContent(),
                                prescriptionToRequest
                                        );
                
                DataAccess.getData().getSecretaryRequests().add(requestMeds);
                
                DataAccess.
                    getData().
                    findPatient(
                            DataAccess.
                                    getData().
                                    getCurrentUser().
                                    getLogin().
                                    getUserName()
                    ).
                        getPrescriptions().
                        remove(prescriptionToRequest);
                
                setPresciptions();
                
                DataAccess.getData().saveData();
                
            }
            
        }
        
    }
    
    
}
