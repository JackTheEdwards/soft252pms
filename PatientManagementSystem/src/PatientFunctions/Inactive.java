/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PatientFunctions;

import MedicinePackage.Prescription;
import RatingsPackage.RatingsCollection;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author jedwards11
 */
public class Inactive implements PatientStatus{

    /**UNIMPLEMENTED
     *
     */
    @Override
    public void requestAccountCreation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**giveFeedback(String doctorID, int rating, String feedback)
     *
     * performs no action due to inactive status
     * 
     * @param doctorID
     * @param rating
     * @param feedback
     */
    @Override
    public void giveFeedback(String doctorID, int rating, String feedback) {
    }

    /**viewDoctorRating(String doctorID)
     *
     * performs no action due to inactive status
     * 
     * @param doctorID
     * @return
     */
    @Override
    public RatingsCollection viewDoctorRating(String doctorID) {
        return null;
    }

    /**viewHistory()
     *
     * performs no action due to inactive status
     * 
     * @return null
     */
    @Override
    public List<String> viewHistory() {
        return null;
    }

    /**requestAppointment()
     *
     * performs no action due to inactive status
     * 
     * @param apptmtTime
     * @param DoctorID
     */
    @Override
    public void requestAppointment(Timestamp apptmtTime, String DoctorID) {
    }

    /**viewPrescriptions()
     *
     * performs no action due to inactive status
     * 
     * @return null
     */
    @Override
    public List<Prescription> viewPrescriptions() {
        return null;
    }

    /**
     *
     * performs no action due to inactive status
     */
    @Override
    public void requestTermination() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**requestPrescription()
     *
     * performs no action due to inactive status
     * 
     * @param prescription
     */
    @Override
    public void requestPrescription(Prescription prescription) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
