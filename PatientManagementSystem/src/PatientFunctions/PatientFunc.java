/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PatientFunctions;

import HospitalAdmin.DataAccess;
import MedicinePackage.Prescription;
import RatingsPackage.RatingsCollection;
import UserPackage.PatientDetails;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author jedwards11
 */
public class PatientFunc {
    
    private PatientStatus status;

    /**setStatus()
     *
     * gets the current status of the patient's account, then sets an active or inactive state depending
     * on which status the patientDetails currently shows
     * 
     */
    public void setStatus(boolean status){
        
        PatientDetails currentUser = (PatientDetails) DataAccess.getData().getCurrentUser();
        
        currentUser.setStatus(status);
        
        if (currentUser.getStatus()){
            this.status = new Active();
        }else{
            this.status = new Inactive();
        }
    }
    
    /** requestAccountCreation()
     *  
     *  performs an implementation of requestAccountCreation() according to the current status
     */
    public void requestAccountCreation(){
        status.requestAccountCreation();
    }
    
    /** giveFeedback (String doctorID, int rating, String feedback)
     *
     *  performs an implementation of giveFeedback() according to the current status
     * 
     * @param doctorID the ID of the doctor to be reviewed
     * @param rating the rating provided given by the user
     * @param feedback the comments provided by the user 
     */
    public void giveFeedback(String doctorID, int rating, String feedback){
        status.giveFeedback(doctorID, rating, feedback);
    }
    
    /** viewDoctorRating(String doctorID)
     * 
     * performs an implementation of viewDoctorRating according to the current status.
     * returns null if status is inactive
     *
     * @param doctorID the ID of the doctor who's reviews are to be returned
     * @return the reviews to be returned
     */
    public RatingsCollection viewDoctorRating(String doctorID){
        return status.viewDoctorRating(doctorID);
    }
    
    /**viewHistory()
     *
     * performs an implementation of viewHistory according to the current status.
     * returns null if status is inactive
     * 
     * @return the history of the current patient using the system
     */
    public List<String> viewHistory(){
        return status.viewHistory();
    }
    
    public void requestTerminate(){
        status.requestTermination();
    }
            
    
    /** requestAppointment()
     *
     * performs an implementation of requestAppointment according to the current status.
     * 
     */
    public void requestAppointment(Timestamp apptmtTime, String DoctorID){
        status.requestAppointment( apptmtTime,  DoctorID);
    }
    
    /**viewPrescriptions()
     *
     * performs an implementation of requestAppointment according to the current status.
     * returns null if status is inactive
     * 
     * @return the prescriptions of the current patient using the system
     */
    public List<Prescription> viewPrescriptions(){
        return status.viewPrescriptions();
    }
    
    /**UNIMPLEMENTED
     *
     */
    public void requestAccountTermination(){
    }
    
    
    
    
}
