/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SecretaryFunctions;

import GenericForms.RemoveUserForm;
import HospitalAdmin.Controller;
import HospitalAdmin.ControllerStrategy;
import HospitalAdmin.DataAccess;
import HospitalAdmin.InitialController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class SecretaryController extends ControllerStrategy{
    
    SecretaryForm view;
    SecretaryFunc tools;
    
    RemoveUserForm removeView;
    
    private int requestsNumber;
    
    AcceptRequestListener acceptListener;
    DenyRequestListener declineListener;
    NextButtonListener nextBtnListener;
    PreviousButtonListener previousBtnListener;
    LogoutListener logoutListener;
    RemovePatientListener removePatientListener;
    ConfirmRemoveListener confirmRemovePatientListener;
    backButtonRemoveViewListener backButtonRemoveViewListener;
    
    
    
    public SecretaryController(Controller controller) {
        super(controller);
        
        //initializes listeners for 
        acceptListener = new AcceptRequestListener();
        declineListener = new DenyRequestListener();
        nextBtnListener = new NextButtonListener();
        previousBtnListener = new PreviousButtonListener();
        logoutListener = new LogoutListener();
        removePatientListener = new RemovePatientListener();
        
        //set listeners for remove Patient
        confirmRemovePatientListener = new ConfirmRemoveListener();
        backButtonRemoveViewListener = new backButtonRemoveViewListener();
        
        //sets requestsNumber
        requestsNumber = 0;
        
        //iniitalizes forms
        removeView = new RemoveUserForm();
        view = new SecretaryForm();
        
        //initializes tools for secretary functionality
        tools = new SecretaryFunc();
        
        
        if (DataAccess.getData().getSecretaryRequests().size()>0){
            view.setTxtRequestBox(DataAccess.getData().getSecretaryRequests().get(requestsNumber).getMessage());
        }
        
        view.setVisible(true);
        
        //adds relevant listeners to view
        view.addAcceptRequestListen(acceptListener);
        view.addDeclineRequestListen(declineListener);
        view.addNextButtonListener(nextBtnListener);
        view.addPreviousButtonListener(previousBtnListener);
        view.addLogoutListener(logoutListener);
        view.addPatientRemoveListener(removePatientListener);
        
        //adds relevant listeners to removeView
        removeView.addConfirmListener(confirmRemovePatientListener);
        removeView.addBackButtonListener(backButtonRemoveViewListener);
        
    }
    
    
    class AcceptRequestListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(DataAccess.getData().getSecretaryRequests().size() > 0 ){
                tools.respondRequest(true, requestsNumber);
            }
            
            if (requestsNumber>DataAccess.getData().getSecretaryRequests().size()){
                requestsNumber = DataAccess.getData().getSecretaryRequests().size();
            }
            
            if (DataAccess.getData().getSecretaryRequests().size()>0){
                view.setTxtRequestBox(DataAccess.getData().getSecretaryRequests().get(requestsNumber).getMessage());
            } else{
                view.setTxtRequestBox("");
            }
            
        }
        
    }
    
    
    class DenyRequestListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(DataAccess.getData().getSecretaryRequests().size() > 0 ){
                tools.respondRequest(false, requestsNumber);
            }
            
            if (requestsNumber>=DataAccess.getData().getSecretaryRequests().size()){
                requestsNumber = DataAccess.getData().getSecretaryRequests().size() - 1;
            }
            
            if (DataAccess.getData().getSecretaryRequests().size()>0){
                view.setTxtRequestBox(DataAccess.getData().getSecretaryRequests().get(requestsNumber).getMessage());
            } else{
                view.setTxtRequestBox("");
            }
        }
        
    }
    
    class NextButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if (requestsNumber < DataAccess.getData().getSecretaryRequests().size()-1 ){
                requestsNumber += 1;
                view.setTxtRequestBox(DataAccess.getData().getSecretaryRequests().get(requestsNumber).getMessage());
            }
            
        }
        
    }
    
    class LogoutListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            getController().setStrategy(new InitialController(getController()));
            
            view.setVisible(false);
            view.dispose();
            removeView.dispose();
        }
        
    }
    
    class PreviousButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if ((requestsNumber>0)  && (DataAccess.getData().getSecretaryRequests().size() > 0 )){
                requestsNumber -= 1;
                view.setTxtRequestBox(DataAccess.getData().getSecretaryRequests().get(requestsNumber).getMessage());
            }
            
        }
        
    }
    
    class RemovePatientListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            view.setVisible(false);
            
            removeView.setStaffComboBox(DataAccess.getData().getUserGroup("Patient"));
            
            removeView.setVisible(true);
            
            
            
        }
        
    }
    
    class ConfirmRemoveListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            String userToRemove = "No ussers found ";
            
            try{
                userToRemove = removeView.getComboBoxContents();
            
                System.out.println("confirm");
                
                tools.removePatient(userToRemove);
            } catch (NullPointerException nullPoint) {
                
            }
            
            JOptionPane.showMessageDialog(new JFrame(),"Patient " + userToRemove + " was removed from the roster") ;
            
            view.setVisible(true);
            
            removeView.setVisible(false);
        }
        
    }
    
    class backButtonRemoveViewListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            System.out.println("back");
            
            view.setVisible(true);
            
            removeView.setVisible(false);
            
        }
        
    }
    
    
}
