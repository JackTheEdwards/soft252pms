/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SecretaryFunctions;

import HospitalAdmin.DataAccess;
import MedicinePackage.Prescription;
import NotificationsPackage.Request;
import UserPackage.PatientDetails;

/**
 *
 * @author User
 */
public class SecretaryFunc {
    
    public void removePatient(String patientID){
        
        PatientDetails patientToRemove = DataAccess.getData().findPatient(patientID);
        
        DataAccess.getData().getUsers().remove(patientToRemove);
        DataAccess.getData().saveData();
    }
    
    public void giveMedicine(Prescription prescription, String recipient){
        
        DataAccess.getData().getMedCab().giveMedicine(prescription, recipient);
        DataAccess.getData().saveData();
    }
    
    public void respondRequest(Boolean Response, int requestID){
        
        DataAccess.getData().getSecretaryRequests().get(requestID).respondRequest(Response);
        DataAccess.getData().saveData();
    }
    
    
    
    
}
