/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package RatingsPackage;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**class that contains all the review information for a given object, including
 * all past reviews, and a average (mean) score of all the reviews
 *
 * @author jedwards11
 */
public class RatingsCollection implements Serializable{
    
    private List<Review> reviews = new ArrayList<Review>();
    private int avgRating;

    /**getReviews()
     *
     * accessor method for reviews
     * 
     * @returns the list of reviews given
     */
    public List<Review> getReviews() {
        return reviews;
    }
    
    
    /**calculateRating()
     * 
     * calculates the average score based upon the reviews in the collection
     *
     * @return the rating of the reviews
     */
    private double calculateRating(){
        double rating = 0;
        int i = 0;
        while ( i < reviews.size() ){
            
            rating += reviews.get(i).getRating();
            
            i++;
        
        }
        rating = rating/(double)i;
        
        return rating;
    }
    
    /**getAverageRating()
     * 
     * Returns a string displaying the average percent that the subject is rated, 
     * based upon the reviews the subject has received. if the subject has no reviews,
     * returns "No reviews available"
     *
     * @return avgReturnMsg the string containing the calculated average review score, 
     *                      or the message "No reviews available", if no reviews are 
     *                      available
     */
    public String getAverageRating(){
        
        DecimalFormat df = new DecimalFormat("0.00##");
        String ratingFormatted = df.format(calculateRating());
        
        String avgReturnMsg ="Rating: "+  ratingFormatted + "%";
        
        
        
        return avgReturnMsg;
    }
    
    /**addReview()
     * 
     * adds the review that was passed as a parameter to the collection for the given subject
     *
     * @param reviewToAdd the review to be added to the collection
     */
    public void addReview (Review reviewToAdd){
        
        reviews.add(reviewToAdd);
        
    }
    
    /**removeReview()
     * 
     * removes the review passed as a parameter from the collection
     *
     * @param reviewToRemove
     */
    public void removeReview(Review reviewToRemove){
        
        reviews.remove(reviewToRemove);
        
    }
    
    /**findReview()
     *
     * returns the review from the collection that has the same index as the number parsed 
     * 
     * @param index the specific index of the review to be retrieved
     * @return the review to be returned
     */
    public Review findReview(int index){
        Review review = new Review("","",0);
        
        
        return review;
    }
    
    public Review findReview (String DateTimeString){
        
        Review reviewToReturn = null;
        
        for (Review review : getReviews()){
            if (DateTimeString.equals( review.getReviewDate().toString() ) ){
                
                return review;
                
            }
            
            
        }
        
        return reviewToReturn;
    }
    
    /**Testing method to access private method calculateRating
     * 
     * Status: ACTIVE (comment out when testing is complete)
     *
     */
    public double CalculateRatingTest(){
        return calculateRating();
    }
    
}
