/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package RatingsPackage;


import java.io.Serializable;
import java.sql.Timestamp;
/**Review class
 * 
 * Class that contains all relevant information for a review, 
 * including the specific feedback, the author of the review, 
 * the rating (limited to integers between 0 and 100), and the 
 * time of the review
 *
 * @author jedwards11
 */
public class Review implements Serializable{
    private String feedback;
    private String author;
    private int rating;
    private Timestamp reviewDate;

    public Review(String feedback, String author, int rating) {
        this.feedback = feedback;
        if (rating > 100 || rating < 0){
            throw new IllegalArgumentException("Rating must be between 0 and 100");
        }
        reviewDate = new Timestamp(System.currentTimeMillis());
        this.author = author;
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public Timestamp getReviewDate() {
        return reviewDate;
    }

    public String getFeedback() {
        return feedback;
    }

    public int getRating() {
        return rating;
    }
    
    
    
}
