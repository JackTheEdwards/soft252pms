/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HospitalAdmin;

import GenericForms.CreatePatientAccountForm;
import NotificationsPackage.RequestCreateAccount;
import PatientFunctions.PatientBuilder;
import UserPackage.PatientDetails;
import UserPackage.Sex;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class NewPatientController extends ControllerStrategy{

    CreateAccountListener makeAccountListener;
    
    CreatePatientAccountForm view;
    
    /**NewPatientController()
     * 
     * constructor method for NewPatientController
     *
     * @param controller the program controller
     */
    public NewPatientController(Controller controller) {
        super(controller);
        
        makeAccountListener = new CreateAccountListener();
        
        view = new CreatePatientAccountForm();
        
        view.setVisible(true);
        
        view.addCreateAccountButtonListener(makeAccountListener);
        
    
        
    }
    
    /**LoginListener
     *
     * ActionListener for creating a patient account
     */
    class CreateAccountListener implements ActionListener{

        /**actionPerformed()
        *
        *creates new patient based upon the input details 
        * 
        * @param e the action
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            PatientBuilder builder = PatientBuilder.newInstance();
            
            builder.setAddress(view.getAddress());
            
            builder.setGivenName(view.getGivenName());
            
            if(view.getSex().equals("Female"))
            {
                builder.setSex(Sex.FEMALE);
            }else
            {
                builder.setSex(Sex.MALE);    
            }
            
            
            builder.setDateOfBirth(view.getDateOfBirth());
            
            builder.setLogin(view.getPassword());
            
            builder.setSurname(view.getSurname());
            
            PatientDetails newPatient = builder.build();
            
            
            System.out.println (DataAccess.getData().getUsers().toString());
            
            DataAccess.getData().getUsers().add(newPatient);
            
            System.out.println (DataAccess.getData().getUsers().toString());
            
            
            RequestCreateAccount newAccountRequest = new RequestCreateAccount(newPatient, "Request To Enable Account " + builder.getGivenName() + " " + builder.getSurname());
            
            
            DataAccess.getData().getSecretaryRequests().add(newAccountRequest);
            
            JOptionPane.showMessageDialog(new JFrame(),"New Account Created,  Username \""+
                    builder.getLogin().getUserName() +
                    "\", Password: \"" +
                    builder.getLogin().getPassWord() +
                    "\" Please wait for account approval before attempting to log in");
            
            
            DataAccess.getData().saveData();
            
            getController().setStrategy(new InitialController(getController()));
            
            
            
            view.setVisible(false);
            view.dispose();
        }
        
    }

    
    
    
}
