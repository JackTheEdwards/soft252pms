/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HospitalAdmin;

import AdminFunctions.AdminController;
import DoctorFunctions.DoctorController;
import GenericForms.LoginScreenForm;
import LoginPackage.LoginController;
import LoginPackage.LoginDetails;
import PatientFunctions.PatientController;
import SecretaryFunctions.SecretaryController;
import UserPackage.AdminDetails;
import UserPackage.DoctorDetails;
import UserPackage.PatientDetails;
import UserPackage.SecretaryDetails;
import UserPackage.User;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class InitialController extends ControllerStrategy{

    LoginScreenForm view = new LoginScreenForm();
    
    LoginController doorman = new LoginController();

    /**InitialController()
     *
     * constructor method for InitialController
     * 
     * @param controller the program controller
     */
    public InitialController(Controller controller) {
        super(controller);
        
        DataAccess.getData().loadData();
        
        this.view = new LoginScreenForm();
        view.setVisible(true);
        
        this.view.addLoginButtonListener(new LoginListener());
        
        this.view.addNewAcountListener(new createAccountListener());
    }
    
    
   
    
    /**LoginListener
     *
     * ActionListener for handling login
     */
    class LoginListener implements ActionListener{
        
        
        /**actionPerformed()
        *
        * handles the login proccess for the system, logging in users to appropriate parts of the system if the login details 
        * provided are correct, and displaying an appropriate error message if not
        * 
        * @param e the action
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String username = view.getUsernameFromInput();
            String password = view.getPasswordFromInput();
            
            System.out.println(password);
            
            LoginDetails loginAttempt = new LoginDetails(username, password);
            
            String loginAttemptStatus = doorman.checkUser(loginAttempt, DataAccess.getData().getLoginDetails());
            
            
            
            if(loginAttemptStatus.equals("Login Successful") ){
                
                DataAccess.getData().setCurrentUser(loginAttempt.getUserName());
                
                User currentUser = DataAccess.getData().getCurrentUser();
                
                
                
                if (DataAccess.getData().getCurrentUser()instanceof AdminDetails) {
                    
                    getController().setStrategy(new AdminController(getController(), true));
                    view.setVisible(false);
                    view.dispose();
                    
                } else if (DataAccess.getData().getCurrentUser()instanceof SecretaryDetails){
                        
                    getController().setStrategy(new SecretaryController(getController()));
                    view.setVisible(false);
                    view.dispose();
                    
                } else if (DataAccess.getData().getCurrentUser()instanceof DoctorDetails){
                    
                    getController().setStrategy(new DoctorController(getController()));
                    view.setVisible(false);
                    view.dispose();
                    
                } else if (DataAccess.getData().getCurrentUser()instanceof PatientDetails){
                    
                    getController().setStrategy(new PatientController(getController()));
                    view.setVisible(false);
                    view.dispose();
                    
                }
                
                
            }else{
                JOptionPane.showMessageDialog(new JFrame(),loginAttemptStatus ) ;
            }
            
            
            
        }
        
    }
    
    /**LoginListener
     *
     * ActionListener for handling CreateAccount
     */
    class createAccountListener implements ActionListener{

        
        /**actionPerformed()
        *
        * switches to the newPatientController if there are currently users in the system, and if not,
        * swtiches to adminController to add an intial admin account
        * 
        * @param e the action
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            if (DataAccess.getData().getUsers().isEmpty()){
                
                getController().setStrategy(new AdminController(getController(), false));
                
            }else{
            
                getController().setStrategy(new NewPatientController(getController()));
            
            }
            view.setVisible(false);
            view.dispose();
        }
        
    }
    
    
}
