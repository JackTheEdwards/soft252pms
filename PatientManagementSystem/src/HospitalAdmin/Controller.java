 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HospitalAdmin;

/**
 *
 * @author User
 */
public class Controller {
    
    ControllerStrategy strategy = new ControllerStrategy(this);
    
    /**Controller()
     *
     * Constructor method
     * 
     * @param strategy the strategy to set
     */
    public Controller(ControllerStrategy strategy) {
        
        this.strategy = strategy;
        
    }

    /**Controller()
     * 
     * Constructor method
     */
    public Controller() {
    }
    
    /**setStrategy
     *
     * setter method for strategy field
     * 
     * @param strategy
     */
    public void setStrategy(ControllerStrategy strategy) {
        this.strategy = strategy;
    }
    
    
    
}
