/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HospitalAdmin;
import AppointmentPackage.Appointment;
import java.util.List;
import java.util.ArrayList;

import UserPackage.*;
import ReadWritePackage.*;
import NotificationsPackage.*;

import MedicinePackage.MedicineCabinet;
import LoginPackage.LoginController;
import LoginPackage.LoginDetails;
import java.io.File;
/**
 *
 * @author User
 */
public class DataAccess {
    
    private static DataAccess data = null;
    
    private User currentUser;
    
    private String savePath = "src\\data";
    
    private List<User> users = new ArrayList<User>();
    private List<Request> secretaryRequests = new ArrayList<Request>();

    private MedicineCabinet medCab = new MedicineCabinet();
    
    
    
    private DataAccess(){
        
    }

    /**loadData()
     *
     * Loads the data from the files inside savePath
     */
    public void loadData(){
        Reader fileInput = new Reader(savePath);
        
        System.out.println("Reading users");
        if (! fileInput.readObject("users").equals( null )){
            users = (List<User>) fileInput.readObject("users");
        }else {
            
            medCab = new MedicineCabinet();
            
        }
        
        System.out.println("Reading requests");
        if (! fileInput.readObject("requests").equals( null )){
            
            secretaryRequests = (List<Request>) fileInput.readObject("requests");
        }
        
        System.out.println("Reading medicine cabinet");
        if (! (fileInput.readObject("medCab") == null)){
            
            medCab = (MedicineCabinet) fileInput.readObject("medCab");
            
        }else {
            
            medCab = new MedicineCabinet();
            
        }
        
    }
    
    /**saveData()
     *
     * saves the user, secretaryRequests and medCab fields into documents in savePath
     */
    public void saveData(){
        Writer fileOutput = new Writer(savePath);
        
        fileOutput.writeObject(users, "users");
        fileOutput.writeObject(secretaryRequests, "requests");
        fileOutput.writeObject(medCab, "medCab");
        
        
        
    }
    /**getCurrentUser()
     *
     * accessor method for the currentUser field
     * 
     * @return
     */
    public User getCurrentUser() {
        return currentUser;
    }
    
    /**findUser(String userID)
     * 
     * finds a user with a specified ID. if no user is found, returns null
     * Note: will need casting before accessing any specific fields eg(Patient Notes, Doctor reviews etc)
     *
     * @param userID the user ID of the the user to return
     * @return to user to be found
     */
    public User findUser(String userID){
        
        User userToReturn = null;
        
        for (User user:getUsers()){
            if (user.getLogin().getUserName().equals( userID)){
                
                userToReturn = user;
                
                break;
            }
        }
        
        return userToReturn;
    }
    
    /**findDoctor (String userID){
     *
     * finds a doctor with a specific id, and returns that doctor.
     * returns null if no doctor is found
     * 
     * @param userID the user ID of the doctor to be returned.
     * @return the doctor to be returned
     */
    public DoctorDetails findDoctor (String userID){
        
        DoctorDetails doctorToReturn = null;
        
        doctorToReturn = (DoctorDetails)findUser(userID);
        
        return doctorToReturn;
        
    }

    /** findSecretaray (String userID)
     *
     * finds a secretary with the specified ID, returns that secretary
     * returns null if no doctor is found
     * 
     * @param userID the user ID of the secretary to be returned.
     * @return the secretary to be returned
     */
    public SecretaryDetails findSecretary (String userID){
        
        SecretaryDetails secretaryToReturn = null;
        
        secretaryToReturn = (SecretaryDetails)findUser(userID);
        
        return secretaryToReturn;
        
    }

    /** find Admin (String userID)
     * 
     * finds an Admin with the specified ID, returns that secretary
     * returns null if no doctor is found
     * 
     * @param userID the user ID of the admin to be returned.
     * @return the admin to be returned
     */
    public AdminDetails findAdmin (String userID){
        
        AdminDetails adminToReturn = null;
        
        adminToReturn = (AdminDetails)findUser(userID);
        
        return adminToReturn;
        
    }

    /** findPatient (String userID)
     *
     * finds the patient details with the specified ID, returns that secretary
     * returns null if no doctor is found
     * 
     * @param userID
     * @return
     */
    public PatientDetails findPatient (String userID){
        
        PatientDetails patientToReturn = null;
        
        patientToReturn = (PatientDetails)findUser(userID);
        
        return patientToReturn;
        
    }
   
    /** getData()
     * 
     * Singleton Method for DataAccess
     *
     * @return the DataAccess class
     */
    public static DataAccess getData(){
        if (data == null){
            data = new DataAccess();
        }
        return data;
    }

    /**getUsers()
     *
     * accessor method for the users field
     * 
     * @return users field
     */
    public List<User> getUsers() {
        return users;
    }

    /**getMedCab()
     *
     * accessor method for the medCab field
     * 
     * @return medCab
     */
    public MedicineCabinet getMedCab() {
        return medCab;
    }
    
    /**getSecretaries()
     * 
     * returns a list of the secretaries 
     *
     * @return list of secretary users
     */
    public List<User> getSecretaries(){
        
        List<User> secretaries = new ArrayList<User>();
        
        for (User possibleSecretary:getUsers()){
            
            if (possibleSecretary instanceof SecretaryDetails){
                secretaries.add(possibleSecretary);
            }
            
        }
        
        return secretaries;
        
    }

    /**getSecretaryRequests
     *
     * accessor method for secretaryRequests
     * 
     * @return secretaryRequests
     */
    public List<Request> getSecretaryRequests() {
        return secretaryRequests;
    }
    
    /**getLoginDetails()
     * 
     * returns a list of current user logins
     *
     * @return a list of login details of users currently in the system
     */
    public List<LoginDetails> getLoginDetails(){
        
        List<LoginDetails> logins = new ArrayList<LoginDetails>();
        
        for (User login: getUsers()){
            logins.add(login.getLogin());
        }
        
        return logins;
    }

    /**setCurrentUser()
     * 
     * setter method for current user
     *
     * @param userID the ID of the current User
     */
    public void setCurrentUser(String userID) {
        
        
        
        this.currentUser = (User)DataAccess.getData().findUser(userID);
        System.out.println(userID);
        
        System.out.println(DataAccess.getData().findUser(userID).getLogin().getUserName());
        
        System.out.print(DataAccess.getData().currentUser.getLogin().getUserName());
    }
    
    /**getUserGroup(0
     * 
     * gets a List of strings containing the username of a specified group
     *
     * @param userType the user type to get
     * @return the list of user id
     */
    public List<String> getUserGroup (String userType){
        
        List<String> usersToGet = new ArrayList<String>();
        
        for (User user:getUsers()){
            
            
            switch (userType){
                case "Doctor":
                        
                        if (user instanceof DoctorDetails){
                            
                            usersToGet.add(user.getLogin().getUserName());
                            
                        }
                        
                        break;
                case "Admin":
                    
                        if (user instanceof AdminDetails){
                            
                            usersToGet.add(user.getLogin().getUserName());
                            
                        }
                        
                    break;
                    
                case "Secretary":
                    
                    if (user instanceof SecretaryDetails){
                            
                            usersToGet.add(user.getLogin().getUserName());
                            
                        }
                    
                    
                    break;
                    
                case "Patient":
                    
                    if (user instanceof PatientDetails){
                            
                            usersToGet.add(user.getLogin().getUserName());
                            
                        }
                    
                    break;
            }
            
        }
        
        return usersToGet;
    }
    
    
    
}
