/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HospitalAdmin;

import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class ControllerStrategy {

    
    private Controller controller;
    
    /**getController()
     * 
     * getter method for controller field
     *
     * @return the current controller
     */
    public Controller getController() {
        return controller;
    }
    
    
    

    /**ControllerStrategy()
     * 
     * Constructor method for Controller Strategy
     *
     * @param controller the current controller
     */
    public ControllerStrategy(Controller controller) {
        this.controller = controller;
    }
    
    
    
}
