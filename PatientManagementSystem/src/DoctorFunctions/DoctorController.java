/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoctorFunctions;

import HospitalAdmin.Controller;
import HospitalAdmin.ControllerStrategy;
import HospitalAdmin.DataAccess;
import HospitalAdmin.InitialController;
import MedicinePackage.Medicine;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class DoctorController extends ControllerStrategy{
    
    //sets a new array of the forms used in doctor functionality
    DoctorForm Forms[] = new DoctorForm[4];
    
    //creates an instance of DoctorFunc to use
    DoctorFunc tools;
    
    //sets references for each form 
    final int genericRef = 0;
    final int medicineRef = 1;
    final int patientNotesRef = 2;
    final int appointmentsRef = 3;
    int currentForm;
    
    int currentMessage  =  0;
    
    
    
    RequestStockMedicineListener stockMedicineListener;
    CreateMedicineListener createMedicineListener;
    
    MedicineMenuListener medMenuListener ;
    PatientNotesListener patientNotesMenuListener ;
    LogoutListener logoutListener ;
    AppointmentsMenuListener appointmentsMenuListener; 
    NextMessageListener nextMessageListener;

    /**setNotification()
     * 
     * sets the current notification to one specified by notificationIndex
     *
     * @param notificationIndex the index of the notification to get
     */
    public void setNotification(int notificationIndex){
        for (int i = 0; i < Forms.length; i++){
            if (DataAccess.getData().getCurrentUser().getReceivedNotifications().isEmpty()){
                Forms[i].setNotification("");
            } else{
                Forms[i].setNotification(DataAccess.getData().getCurrentUser().getReceivedNotifications().get(notificationIndex).getMessage());
            }
            
            
        }
    }
    
    /**setBlankNotification
     *
     * sets notification to blank
     * 
     */
    public void setBlankNotification (){
        for (int i = 0; i < Forms.length; i++){
            Forms[i].setNotification("");
        }
    }
    
    /**DoctorController()
     * 
     * sets up the controller 
     *
     * @param controller
     */
    public DoctorController(Controller controller) {
        super(controller);
        
        tools = new DoctorFunc();
        
        //initializes each form 
        Forms[genericRef] = new DoctorForm();
        Forms[medicineRef] = new DoctorMedicineForm();
        Forms[patientNotesRef] = new DoctorPatientNotesForm();
        Forms[appointmentsRef] = new DoctorAppointmentsForm();
        
        
        //initialize listeners
        MedicineMenuListener medMenuListener = new MedicineMenuListener();
        PatientNotesListener patientNotesMenuListener = new PatientNotesListener();
        LogoutListener logoutListener = new LogoutListener();
        AppointmentsMenuListener appointmentsMenuListener = new AppointmentsMenuListener();
        NextMessageListener nextMessageListener = new NextMessageListener();
        DeleteMessageListener deleteMessageListener  = new DeleteMessageListener();
        
        RequestPrescriptionListener requestPrescriptionListener  = new RequestPrescriptionListener();
        ChangePatientListener changePatientListener = new ChangePatientListener();
        MakeNoteListener makeNoteListener = new MakeNoteListener();
        MakeAppointmentListener makeApptmtListener = new MakeAppointmentListener ();
        
        createMedicineListener  = new CreateMedicineListener();
        stockMedicineListener = new RequestStockMedicineListener();
        
        //sets the current form to the doctor generic one
        currentForm = genericRef;
        
        Forms[currentForm].setVisible(true);
        
        //adds generic listeners to all forms
        for (int i =0; i < Forms.length; i++){
            Forms[i].addBtnAppointmentsListener(appointmentsMenuListener);
            Forms[i].addBtnMedicineListener(medMenuListener);
            Forms[i].addBtnPatientListener(patientNotesMenuListener);
            Forms[i].addBtnLogoutListener(logoutListener);
            Forms[i].addBtnDeleteMessageListner(deleteMessageListener);
            Forms[i].addBtnNextMessageListener(nextMessageListener);
        }
        
        //adds listeners to patientNotes form
        Forms[patientNotesRef].addBtnRequestPrescriptionListener(requestPrescriptionListener);
        Forms[patientNotesRef].addChangePatientListener(changePatientListener);
        Forms[patientNotesRef].addMakeAppointmentListener(makeApptmtListener);
        Forms[patientNotesRef].addMakeNoteListener(makeNoteListener);
        
        //adds listeners to medicine screen
        Forms[medicineRef].addRequestStockMedicineListener(stockMedicineListener);
        Forms[medicineRef].addCreateMedicineListener(createMedicineListener);
        
        setNotification(currentMessage);
        
    }
    
    /**hideForms()
     *
     * hides all forms
     * 
     */
    public void hideForms(){
        for (int i =0; i < Forms.length; i++){
            
            Forms[i].setVisible(false);
            
        }
    }
    
    /** showForm()
     *
     * shows the selected form
     * 
     * @param selectedForm the form to show
     */
    public void showForm(int selectedForm){
        hideForms();
        currentForm = selectedForm;
        Forms[selectedForm].setVisible(true);
    }
    
    /*Action listener for medicine menu
    *
    */
    class MedicineMenuListener implements ActionListener{

        /*actionPerformed()
        * 
        *  sets medicine menu as the active form
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            showForm(medicineRef);
            
            Forms[medicineRef].setMedicineComboBox(DataAccess.getData().getMedCab().getMedicineNames());
        }
        
    }
    
    /*Action listener for logout button
    *
    */
    class LogoutListener implements ActionListener{

        /*actionPerformed()
        * 
        *  logs out the user, disposes of forms, sets controller strategy to initialController
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            getController().setStrategy(new InitialController(getController()));
            
            hideForms();
            for (int i =0; i < Forms.length; i++){
                Forms[i].dispose();
            }
            
        }
        
    }
    
    /* Action listener for Patient Notes button
    *
    */
    class PatientNotesListener implements ActionListener{

        /*actionPerformed()
        * 
        *  sets relevant fields, shows patientNotesForm
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            showForm(patientNotesRef);
            
            Forms[patientNotesRef].setPatientList(DataAccess.getData().getUserGroup("Patient"));
            Forms[patientNotesRef].setPatientNotes(tools.viewNotes(Forms[patientNotesRef].getCurrentPatient()));
            Forms[patientNotesRef].setMedicineList(DataAccess.getData().getMedCab().getMedicineNames());
        }
        
    }
    
    /* Action listener for Appointments menu
    *
    */
    class AppointmentsMenuListener implements ActionListener{

        /*actionPerformed()
        * 
        *  sets relevant fields, shows appointmentsForm
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            showForm(appointmentsRef);
            Forms[appointmentsRef].setAppointments(tools.viewUpcomingAppointments());
        }
        
    }
    
    /* Action listener for requesting medicine
    *
    */
    class RequestStockMedicineListener implements ActionListener{

        /*actionPerformed()
        * 
        *  request medicine, using relevant input fields as parameters
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String medName = Forms[currentForm].getMedicineComboBoxContents();
            int amountToStock = Forms[currentForm].getMedicineStockAmount();
            
            tools.requestMedicineStock(medName, amountToStock);
            JOptionPane.showMessageDialog(new JFrame(),"Request to stock "+ amountToStock + " of " + medName);
            
        }
        
    }
    
    /* Action listener for creating medicine
    *
    */
    class CreateMedicineListener implements ActionListener{

        /*actionPerformed()
        * 
        *  creates medicine using getNewMedicineText as input
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            tools.createMedicine(Forms[currentForm].getNewMedicineText());
            
            JOptionPane.showMessageDialog(new JFrame(),"Created Medicine :" + Forms[currentForm].getNewMedicineText());
            
            Forms[medicineRef].setMedicineComboBox(DataAccess.getData().getMedCab().getMedicineNames());
        }
        
    }
    
    /* Action listener for creating a prescription
    *
    */
    class RequestPrescriptionListener implements ActionListener{

        /*actionPerformed()
        * 
        *  creates a prescription for a specified user using relevant inputs
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            Medicine medsToPrescribe = new Medicine(Forms[patientNotesRef].getMedicineName(), Forms[patientNotesRef].getMedicineAmount());
            
            tools.createPrescription(
                    medsToPrescribe,
                    Forms[patientNotesRef].getPrescriptionInstruction(),
                    Forms[patientNotesRef].getCurrentPatient()
                    );
                    
        }
        
    }
    
    /*Action listener for 
    *
    */
    class ChangePatientListener implements ActionListener{

        /*actionPerformed()
        * 
        *  sets the patientNotes based upon the selected patient
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String currentPatientID = Forms[currentForm].getCurrentPatient();
            Forms[currentForm].setPatientNotes(tools.viewNotes(currentPatientID));
            
        }
        
    }
    
    /*Action listener for making notes
    *
    */
    class MakeNoteListener implements ActionListener{
        
        /*actionPerformed()
        * 
        *  makes a note on the current patient selected
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String newNote = Forms[currentForm].getNewNote();
            String patientID = Forms[currentForm].getCurrentPatient();
            
            tools.makeNote(patientID, newNote);
            
            Forms[patientNotesRef].setPatientNotes(tools.viewNotes(Forms[patientNotesRef].getCurrentPatient()));
        }
        
    }
    
    /*Action listener for making appointments
    *
    */
    class MakeAppointmentListener implements ActionListener{

        /*actionPerformed()
        * 
        *  sends a request to make an appointment with the selected patient at the selected time
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            Timestamp appointmentTime = Forms[currentForm].getAppointmentTime();
            
            String patientID = Forms[currentForm].getCurrentPatient();
            tools.requestAppointment(appointmentTime, patientID);
        }
        
        
        
    }
    
    /*Action listener for the next message
    *
    */
    class NextMessageListener implements ActionListener{

        /*actionPerformed()
        * 
        * shows the next message if current message is not the final one
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            if (currentMessage < DataAccess.getData().getCurrentUser().getReceivedNotifications().size()-1){
                currentMessage++;
            }
            
            setNotification(currentMessage);
        }
        
    }
    
    /*Action listener for deleting messages
    * 
    * deletes the current message
    */
    class DeleteMessageListener implements ActionListener{

        /*
        *deletes the current message
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            if (DataAccess.getData().getCurrentUser().getReceivedNotifications().isEmpty() == false){
                DataAccess.getData().getCurrentUser().getReceivedNotifications().remove(currentMessage);
            }
            
            setNotification(currentMessage);
            
        }
        
    }
    
    
}
