/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoctorFunctions;

import java.awt.event.ActionListener;
import java.util.List;
import java.sql.Timestamp;
/**
 *
 * @author User
 */
public interface DoctorSpecificListenerInterface {
    
    public void addRequestStockMedicineListener(ActionListener requestStockMedicineListener);
    public void addCreateMedicineListener (ActionListener createMedicineListener);
    public String getMedicineComboBoxContents ();
    public String getNewMedicineText();
    public int getMedicineStockAmount();
    public void setMedicineComboBox(List<String> MedicineList);
    
    public void addBtnRequestPrescriptionListener(ActionListener requestPrescriptionListener);
    public void addChangePatientListener (ActionListener changePatientListener);
    public void addMakeNoteListener(ActionListener makeNoteListener);
    public void addMakeAppointmentListener(ActionListener makeApptmtListener);
    public String getPrescriptionInstruction();
    public Timestamp getAppointmentTime();
    public String getMedicineName();
    public int getMedicineAmount();
    public String getNewNote();
    public String getCurrentPatient();
    public void setMedicineList(List<String> medList);
    public void setPatientNotes (List<String> patientNotes);
    public void setPatientList (List<String> patients);
    
    public void setAppointments(List<String> appointments);
    
    
    
    
    
}
