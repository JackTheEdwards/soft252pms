/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoctorFunctions;

import AppointmentPackage.Appointment;
import HospitalAdmin.DataAccess;
import MedicinePackage.Medicine;
import MedicinePackage.Prescription;
import NotificationsPackage.Request;
import NotificationsPackage.RequestAppointment;
import NotificationsPackage.RequestStockMedicine;
import java.util.List;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author jedwards11
 */
public class DoctorFunc {
    
    /**viewAppointments()
     * 
     * returns the appointments of a the current doctor
     *
     * @return the appointments of the current doctor
     */
    public List<Appointment> viewAppointments(){
        
        return DataAccess
                .getData()
                .findDoctor(DataAccess
                        .getData()
                        .getCurrentUser()
                        .getLogin()
                        .getUserName())
                .getDocSchedule()
                .getAppointments();
        
    }
    
    /**viewUpcomingAppointments
     * 
     * returns the appointments of a the current doctor that have not yet happened
     *
     * @returns all appointments that have not yet happened
     */
    public List<String> viewUpcomingAppointments(){
        
        List<String> upcomingAppointments = new ArrayList<String>();
        
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        
        String appointmentDetails;
        
        for (Appointment appointment :viewAppointments()){
            
            if (currentTime.before(appointment.getAppointmentTime())){
                
                appointmentDetails = appointment.getPatient()+ "  " +appointment.getAppointmentTime().toString();
                
                upcomingAppointments.add( appointmentDetails );
            }
        }
        
        return upcomingAppointments;
    }
    
    /**makeNote(String patientID, String newNote)
     *
     * Adds a string to the current patient's notes, the current patient being specified
     * by patientID. also adds on the name of the doctor and the current time
     * 
     * @param patientID the patient to have a note added
     * @param newNote
     */
    public void makeNote(String patientID, String newNote){
        
        Timestamp noteTime = new Timestamp(System.currentTimeMillis());
        
        newNote = newNote +
                " - Note by Dr" +
                DataAccess.getData().getCurrentUser().getGivenName() +
                " " 
                + DataAccess.getData().getCurrentUser().getSurname() +
                " on " +
                noteTime.toString();
        
        DataAccess.getData().findPatient(patientID).getPatientNotes().add(newNote);
        DataAccess.getData().saveData();
    }
    
    /**viewNotes(String patientID)
     *
     * returns the notes of a specified patient
     * 
     * @param patientID the patient whose notes are to be fetched
     * @return the notes of the patient
     */
    public List<String> viewNotes(String patientID){
        return DataAccess.getData().findPatient(patientID).getPatientNotes();
    }
    
    /**requestAppointment()
     * 
     * sends an appointment request with the specified patient and appointment time
     *
     * @param appointmentTime the time of the appointment
     * @param patientID the id of the patient to have an appointment with
     */
    public void requestAppointment(Timestamp appointmentTime, String patientID){
        
        Appointment proposedAppointment = new Appointment(patientID, appointmentTime);
        
        System.out.println(proposedAppointment.toString());
        
        Request apptmtRequest = new RequestAppointment(
        DataAccess.getData().getCurrentUser(),
                "Appointment Request from "+
                DataAccess.getData().getCurrentUser().getLogin().getUserName()+
                " for "+
                patientID+
                " at "+
                appointmentTime,
                proposedAppointment,
        DataAccess.getData().getCurrentUser().getLogin().getUserName()
        );
        
        DataAccess.getData().getSecretaryRequests().add(apptmtRequest);
        DataAccess.getData().saveData();
    }
    
    /** Creates a medicine with the specified name, sets stock to 0
     *
     * @param medicineName the name of the medicine
     */
    public void createMedicine(String medicineName){
        DataAccess.getData().getMedCab().stockMedicine(medicineName, 0);
        DataAccess.getData().saveData();
    }
    
    /**createPrescription()
     *
     * creates a prescription for the specified patient
     * 
     * @param medsToPrescribe the medicines to be prescribed
     * @param instructions the instructions on how to use the medicine
     * @param patientID
     */
    public void createPrescription(Medicine medsToPrescribe, String instructions, String patientID){
        
        Prescription newPrescription = new Prescription(medsToPrescribe, instructions);
        
        DataAccess.getData().findPatient(patientID).getPrescriptions().add(newPrescription);
        DataAccess.getData().saveData();
    }
    
    /**requestMedicineStock()
     *
     * sends a request for a specified medicine
     * 
     * @param medToStockName the medicine to stock
     * @param amountToStock the amount to stock
     */
    public void requestMedicineStock(String medToStockName, int amountToStock){
        
        Medicine medsToStock = new Medicine(medToStockName, amountToStock);
        
        Request medStockRequest = new RequestStockMedicine(DataAccess.getData().getCurrentUser(), 
        "Request for Medicine " + medsToStock.getName(),
        medsToStock        
        );
        
        DataAccess.getData().getSecretaryRequests().add(medStockRequest);
        DataAccess.getData().saveData();
        
    }
}
