/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReadWritePackage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author jedwards11
 */
public class Writer {
    
    private String relativePath;
    
    private FileOutputStream fileWriter;
    private ObjectOutputStream objectWriter;
    
    /**Writer(String path)
     * 
     * Initializes Writer, setting the path to write to 
     *
     * @param The path to write the current group of objects to
     */
    public Writer(String path){
        this.relativePath = path;
    }
    
    /**writeObject(Object dataToWrite, String dataName)
     *
     * Writes an object to the location specified in the constructor, 
     * with a string specified to use as the name of the file
     * 
     * @param dataToWrite the data to be written to the file
     * @param dataName the name of the file to be written to
     */
    public void writeObject(Object dataToWrite, String dataName){
        
        //Setup path of object to write into class
        dataToWrite.getClass().getTypeName();
        
        String actualPath = relativePath+ "\\" + dataName +".txt";
        
        //Initialize Streams 
        try {
            
            fileWriter = new FileOutputStream(actualPath);
            objectWriter = new ObjectOutputStream(fileWriter);
            
        } catch (FileNotFoundException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Write Object to it's text file
        try {
            System.out.println("WriteObject");
            objectWriter.writeObject(dataToWrite);
        } catch (IOException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Close Streams
        try {
            
            objectWriter.close();
            fileWriter.close();
        } catch (IOException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    
    /**writeObject(Object dataToWrite)
     * 
     * Writes an object to the location specified in the constructor, 
     * using the name of the Object as the name of the file. 
     * (acquired with dataToWrite.getClass().getTypeName(), if 
     * needed for Reader file)
     *
     * @param dataToWrite the data to be written to the file
     */
    public void writeObject(Object dataToWrite){
        
        //Setup path of object to write into class
        dataToWrite.getClass().getTypeName();
        
        String actualPath = relativePath+ "\\" + dataToWrite.getClass().getTypeName() +".txt";
        
        //Initialize Streams 
        try {
            
            fileWriter = new FileOutputStream(actualPath);
            objectWriter = new ObjectOutputStream(fileWriter);
            
        } catch (FileNotFoundException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Write Object to it's text file
        try {
            objectWriter.writeObject(dataToWrite);
        } catch (IOException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Close Streams
        try {
            
            objectWriter.close();
            fileWriter.close();
        } catch (IOException ex) {
            
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    
    
    
}
