/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReadWritePackage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author jedwards11
 */
public class Reader {
    
    private String relativePath;
    
    private FileInputStream fileReader;
    private ObjectInputStream objectReader;
    
    /** Reader (String path)
     *
     * Initializes Reader, setting the path which contains the file to be read from
     * 
     * @param path The path to read the current group of objects from
     */
    public Reader (String path){
        this.relativePath = path;
    }
    
    /**readObject(String dataName)
     * 
     * Reads the object specified in dataName from the path specified in the constructor.
     * Use in conjuction with a cast to retrieve the specific object from the file
     * E.G. String car = (String) readObject ("cars"); 
     * 
     * Note that if this class is being used opposite the Writer class, the name of the file
     * maybe given by "ObjectName.getClass().getTypeName()" if no name was specified for the 
     * file that had the object written into it.
     * 
     * @param dataName the name of the file to be read from
     * @return the object read from the file
     */
    public Object readObject(String dataName){
        
        //initalizes an initial returnObject with null, in case the try catch should fail and the method needs to return something
        Object returnObject = null;
        
        
        //sets a path to read from
        String actualPath = relativePath+ "\\" + dataName +".txt";
        
        File fileCheck = new File (actualPath);
        
        if (fileCheck.exists()){
            
                //attempts to initializes the input streams
            try {

                fileReader = new FileInputStream(actualPath);

                objectReader = new ObjectInputStream(fileReader);

            } catch (IOException ex) {

                Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            }


            //attempts to assign the object in the file to returnObject
            try {
                returnObject = objectReader.readObject();
                System.out.println("Read Successful");
            } catch (IOException ex) {
                System.out.println("Read Failed");
                Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                System.out.println("Read Failed");
                Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            }

            //closes the streams
            try {
                fileReader.close();
                objectReader.close();
            } catch (IOException ex) {

                Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            }

            
            
        }
        
        return returnObject;
        
    }
}
