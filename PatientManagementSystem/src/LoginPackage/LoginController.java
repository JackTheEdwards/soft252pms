/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoginPackage;

import java.util.List;

/** LoginController
 *
 * @author jedwards11
 */
public class LoginController {
    
    /**Class Responsible for checking the login details match one of the users provided
     *
     */
    public LoginController(){
        
        
    }
    
    /** checkUser()
     * 
     * Method that returns "Login Successful", "Invalid Password" or "Invalid Username",
     * depending on if any of the details of userToCheck match any of the users passed in"
     *
     * @param userToCheck The User Details to check
     * @param systemUsers The List of currently accepted Users
     * @return "Login Successful", "Invalid Password" or "Invalid Username", depending on a decision tree
     */
    public String checkUser (LoginDetails userToCheck, LoginDetails[] systemUsers){
        String loginStatus = "Invalid Username";
        if (checkUserName(userToCheck.getUserName(), systemUsers )== false)
        {
            return loginStatus;
        }
        else if (checkPassWord(userToCheck, systemUsers) == false)
        {
            loginStatus = "Invalid Password";
            return loginStatus;
        }
        else
        {
            loginStatus = "Login Successful";
        }        
        return loginStatus;
    }
    
    /** checkUser()
     * 
     * Method that returns "Login Successful", "Invalid Password" or "Invalid Username",
     * depending on if any of the details of userToCheck match any of the users passed in"
     *
     * @param userToCheck The User Details to check
     * @param systemUsers The List of currently accepted Users
     * @return "Login Successful", "Invalid Password" or "Invalid Username", depending on a decision tree
     */
    public String checkUser (LoginDetails userToCheck, List<LoginDetails> systemUsers){
        
        LoginDetails[] systemUsersArray = new LoginDetails[systemUsers.size()];
        
        for (int i = 0; i < systemUsersArray.length; i++){
            systemUsersArray[i] = systemUsers.get(i);
        }
        
        return checkUser(userToCheck, systemUsersArray);
        
        
    }
    
    /** checkUserName()
     * 
     * STATUS: "private" owing to successful testing, change to protected if modification is needed
     * 
     * Method that checks a given username against a list of Users to see if there is a 
     * user with those login details
     * 
     * @param Username The Username to check
     * @param systemUsers The list of users to check against the given username
     * @return "True" if found Username is found, "False" if not
     */
    private Boolean checkUserName(String Username, LoginDetails[] systemUsers){
        Boolean isUserPresent = false;
        
        for (int i  = 0; i < systemUsers.length; i++){
            if (systemUsers[i].getUserName().equals(Username)){
                isUserPresent = true;
            }
        }
        
        return isUserPresent;
    }
    
    
    /** checkPassWord
     * 
     * STATUS: "private" owing to successful testing, change to protected if modification is needed
     *  
     * Method that checks the a Users password is identical to the one assigned to the one stored in their name
     * 
     * @param userToCheck the Password to be checked
     * @param systemUsers the list of allowed users
     * @return "True" if passwords match, "False" if not
     */
    private Boolean checkPassWord(LoginDetails userToCheck, LoginDetails[] systemUsers){
        Boolean isPassWordCorrect = false;
        
        int userIndex = findUser( userToCheck.getUserName(), systemUsers );
        
        LoginDetails correctUser = systemUsers[ userIndex ];
        
        String correctPassword = correctUser.getPassWord(); 
        
        if (userToCheck.getPassWord().equals(correctPassword) ){
            isPassWordCorrect = true;
        }
        
        return isPassWordCorrect;
    }
    
    /** findUser()
     * 
     * STATUS: "private" owing to successful testing, change to protected if modification is needed
     * 
     * Returns the index of the user that matches the given Username
     * 
     * @param Username The username
     * @param systemUsers the list of users to check
     * @return an int matching the index of the systemUser with the given Username
     */
    public Integer findUser(String Username, LoginDetails[] systemUsers){
        
        int i = 0; 
        
        for (LoginDetails userDetails: systemUsers){
            
            if (Username.equals( userDetails.getUserName())){
                return i;
            }
            i++;
        }
            
           
        
        return i ;
        
    }
    
    
    
}
