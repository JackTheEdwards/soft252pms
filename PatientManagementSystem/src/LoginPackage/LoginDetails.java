/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoginPackage;

import java.io.Serializable;

/**Class that contains fields for a users username and password,
 * as well as relevant accessor and constructor methods 
 *
 * @author jedwards11
 */
public class LoginDetails implements Serializable{
    
    private String userName;
    private String passWord;
    
    public LoginDetails(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    

}
