/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminFunctions;

import java.awt.event.ActionListener;

/**
 *
 * @author User
 */
public class AdminForm extends javax.swing.JFrame {

    /**
     * Creates new form AdminForm
     */
    public AdminForm() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton5 = new javax.swing.JButton();
        btnCreateAccount = new javax.swing.JButton();
        btnGiveFeedback = new javax.swing.JButton();
        btnViewDoctorReviews = new javax.swing.JButton();
        btnRemoveAccount = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();

        jButton5.setText("jButton5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnCreateAccount.setText("Create Account");

        btnGiveFeedback.setText("Give Feedback");

        btnViewDoctorReviews.setText("View Doctor Reviews");

        btnRemoveAccount.setText("Remove Account");

        btnLogout.setText("Logout");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnViewDoctorReviews, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGiveFeedback, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCreateAccount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRemoveAccount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btnLogout))
                .addContainerGap(235, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCreateAccount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGiveFeedback)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnViewDoctorReviews)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemoveAccount)
                .addGap(37, 37, 37)
                .addComponent(btnLogout)
                .addContainerGap(55, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdminForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdminForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdminForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdminForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdminForm().setVisible(true);
            }
        });
    }
    
    /**forceCreateAdmin()
     *
     * enables features of the GUI based upon a boolean input
     * 
     * @param accountPresent the boolean which decides whether the buttons are enabled
     */
    public void forceCreateAdmin(Boolean accountPresent){
        
        btnGiveFeedback.setEnabled(accountPresent);
        
        btnViewDoctorReviews.setEnabled(accountPresent);
        
        btnRemoveAccount.setEnabled(accountPresent);
        
    }
    
    /** addCreateAccountListener()
     *
     * adds an ActionListener to the btnCreateAccount
     * 
     * @param CreateAccountListener the ACtionListener to add
     */
    public void addCreateAccountListener(ActionListener CreateAccountListener){
        
        btnCreateAccount.addActionListener(CreateAccountListener);
        
        
    }
    
    /**addRemoveAccountListener()
     *
     * adds an ActionListener to btnRemoveAccount
     * 
     * @param RemoveAccountListener the ACtionListener to add
     */
    public void addRemoveAccountListener(ActionListener RemoveAccountListener) {
        
        btnRemoveAccount.addActionListener(RemoveAccountListener);
        
    }
    
    /**addGiveFeedbackListener()
     *
     * adds an ActionListener to btnGiveFeedback
     * 
     * @param GiveFeedbackListener the ACtionListener to add
     */
    public void addGiveFeedbackListener(ActionListener GiveFeedbackListener){
        
        btnGiveFeedback.addActionListener(GiveFeedbackListener);
        
    }
    
    /**addViewDoctorReviews()
     *
     * adds an ActionListener to the btnViewDoctorReviews
     * 
     * @param ViewDoctorReviewListener the ACtionListener to add
     */
    public void addViewDoctorReviews (ActionListener ViewDoctorReviewListener){
        
        btnViewDoctorReviews.addActionListener(ViewDoctorReviewListener);
    
    }
    
    /**addLogoutListener()
     *
     * adds an ActionListener to btnLogout 
     * 
     * @param LogoutListener the ACtionListener to add
     */
    public void addLogoutListener(ActionListener LogoutListener){
        btnLogout.addActionListener(LogoutListener);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateAccount;
    private javax.swing.JButton btnGiveFeedback;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnRemoveAccount;
    private javax.swing.JButton btnViewDoctorReviews;
    private javax.swing.JButton jButton5;
    // End of variables declaration//GEN-END:variables

    
    
}
