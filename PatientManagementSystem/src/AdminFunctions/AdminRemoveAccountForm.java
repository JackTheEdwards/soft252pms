/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminFunctions;

import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.event.ChangeListener;

/**
 *
 * @author User
 */
public class AdminRemoveAccountForm extends AdminForm {

    /**
     * Creates new form AdminRemoveAccountForm
     */
    public AdminRemoveAccountForm() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboBoxStaffSelect = new javax.swing.JComboBox<>();
        btnDeleteAccount = new javax.swing.JButton();
        lblNameAccount = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnDeleteAccount.setText("Delete Account");

        lblName.setText("Name: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName)
                    .addComponent(lblNameAccount)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(comboBoxStaffSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(173, 173, 173)
                        .addComponent(btnDeleteAccount)))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxStaffSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDeleteAccount))
                .addGap(18, 18, 18)
                .addComponent(lblNameAccount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName)
                .addContainerGap(138, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveAccountForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveAccountForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveAccountForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveAccountForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdminRemoveAccountForm().setVisible(true);
            }
        });
    }
    
    /**setStaffComboBox()
     * 
     * sets the content of comboBoxStaffSelect
     *
     * @param comboStaff the content to set
     */
    public void setStaffComboBox(List<String> comboStaff){
        
        comboBoxStaffSelect.removeAllItems();
        
        for (String staffMember:comboStaff){
            comboBoxStaffSelect.addItem(staffMember);
        }
        
    }
    
    /** addComboBoxActionListener()
     *
     * adds an actionListener to comboBoxStaffSelect
     * 
     * @param listenForBoxChange the listener to add
     */
    public void addComboBoxActionListener(ActionListener listenForBoxChange){
        
        comboBoxStaffSelect.addActionListener(listenForBoxChange);
        
    }
    
    /**addDeleteButtonActionListener()
     * 
     * adds an actionListener to btnDeleteAccount
     *
     * @param DeleteAccountListener the listener to add
     */
    public void addDeleteButtonActionListener(ActionListener DeleteAccountListener){
        
        btnDeleteAccount.addActionListener(DeleteAccountListener);
        
    }
    
    /**setlblNameText()
     *
     * sets the content of lblName
     * 
     * @param text the text to set lblName to
     */
    public void setlblNameText (String text){
        
        lblName.setText(text);
        
    }
    
    /**getStaffComboContents()
     *
     * Accessor for the contents of comboBoxStaffSelect
     * 
     * @return the content of comboBoxStaffSelect
     */
    public String getStaffComboContents(){
        return String.valueOf(comboBoxStaffSelect.getSelectedItem());
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteAccount;
    private javax.swing.JComboBox<String> comboBoxStaffSelect;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNameAccount;
    // End of variables declaration//GEN-END:variables
}
