/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminFunctions;

import HospitalAdmin.DataAccess;
import NotificationsPackage.Message;
import NotificationsPackage.Notification;
import RatingsPackage.RatingsCollection;
import UserPackage.*;

/**
 *
 * @author jedwards11
 */
public class AdminFunc {
    
    private StaffFactory staffMaker = new StaffFactory();
        
    /**Creates an Admin Account with staffMaker, then adds that user to the list of users within DataAccess. 
     * Uses the current number of users (gained from DataAccess) as the reference for the int required for the 
     * ID number
     *
     * @param password the password for the new account
     * @param givenName the first name of the user of the new account
     * @param surname the surname of the user of the new account
     * @param address the address of the user of the new account
     */
    public void createAdminAccount(String password, String givenName, String surname, String address){
        User newUser = staffMaker.createAccount(
                Staff.ADMIN,
                password,
                DataAccess.getData().getUsers().size(),
                givenName,
                surname,
                address
        );
        
        DataAccess.getData().getUsers().add(newUser);
        DataAccess.getData().saveData();
    }
   
    /**Creates a Secretary Account with staffMaker, then adds that user to the list of users within DataAccess
     * Uses the current number of users (gained from DataAccess) as the reference for the int required for the 
     * ID number
     * 
     * @param password the password for the new account
     * @param givenName the first name of the user of the new account
     * @param surname the surname of the user of the new account
     * @param address the address of the user of the new account
     */
    public void createSecretaryAccount(String password, String givenName, String surname, String address){
        
        User newUser = staffMaker.createAccount(
                Staff.SECRETARY,
                password,
                DataAccess.getData().getUsers().size(),
                givenName,
                surname,
                address
        );
        
        DataAccess.getData().getUsers().add(newUser);
        DataAccess.getData().saveData();
    }
    
    /**Creates a Doctor Account with staffMaker, then adds that user to the list of users within DataAccess
     * Uses the current number of users (gained from DataAccess) as the reference for the int required for the 
     * ID number
     * 
     * @param password the password for the new account
     * @param givenName the first name of the user of the new account
     * @param surname the surname of the user of the new account
     * @param address the address of the user of the new account
     */
    public void createDoctorAccount(String password, String givenName, String surname, String address){
        
        
        User newUser = staffMaker.createAccount(
                Staff.DOCTOR,
                password,
                DataAccess.getData().getUsers().size(),
                givenName,
                surname,
                address
        );
        
        DataAccess.getData().getUsers().add(newUser);
        
        DataAccess.getData().saveData();
    }
    
    /**removes a user with an id that matches the provided ID
     *
     * @param userIDToRemove the specific ID of the user to remove
     * 
     */
    public void removeAccount(String userIDToRemove){
        
            User userToDelete = DataAccess.getData().findUser(userIDToRemove);
            
            DataAccess.getData().getUsers().remove(userToDelete);
            
        DataAccess.getData().saveData();
    }
    
    /** giveFeedback(String doctorID, String feedback)
     *
     * Sends a notification to a doctor specified by doctorID
     * 
     * @param doctor the doctor to give feedback to
     * @param feedback the message to send
     */
    public void giveFeedback(String doctorID, String feedback){
        
        Notification feedbackMessage = new Message(
            DataAccess.getData().getCurrentUser(),
            feedback
        );
        
        DataAccess.getData().findDoctor(doctorID).addNotification(feedbackMessage);
        
        DataAccess.getData().saveData();
    }
    
    /**gets the reviews for a given doctor, specified by a user ID
     *
     * @param userIDToView the specific ID of the doctor who's reviews are needed
     * @return the ratingsCollection to be viewed
     */
    public RatingsCollection viewDoctorReviews(String userIDToView){
        
        RatingsCollection reviews = null;
        DoctorDetails doctorToView;
        
        reviews = DataAccess.getData().findDoctor(userIDToView).getReviews();
        
        return reviews;
    }
    
}
