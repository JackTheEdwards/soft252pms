/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminFunctions;

import HospitalAdmin.Controller;
import HospitalAdmin.ControllerStrategy;
import HospitalAdmin.DataAccess;
import HospitalAdmin.InitialController;
import LoginPackage.LoginDetails;
import RatingsPackage.Review;
import UserPackage.DoctorDetails;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class AdminController extends ControllerStrategy{
    
    AdminForm view ;
    AdminCreateAccountForm viewCreateAccount;
    AdminRemoveAccountForm viewRemoveAccount;
    AdminGiveFeedbackForm viewGiveFeedback;
    AdminViewReviewsForm viewReviews;
    
    
    GiveFeedbackListener listenerGiveFeedback;
    CreateAccountListener listenerCreateAccount;
    RemoveAccountListener listenerRemoveAccount;
    ViewReviewsListener listenerViewReviews;
    LogoutListener listenerLogout;
    
    FinalizeAccountListener listenerFinalize;
    
    DeleteAccountListener listenerDelete;
    ComboStaffBoxListener listenerComboStaffChange;
    
    SendMessageListener listenerSendMessage;
    
    ComboBoxDoctorListener listenerComboDoctorChange;
    ComboBoxDateListener  listenerComboDateChange;
    
    Boolean accountPresent;
    AdminFunc tools;
    
    private void SetComboBoxToStaff(){
                            
            List<String> staffMembers = new ArrayList<String>();
                    
            staffMembers.addAll(DataAccess.getData().getUserGroup("Doctor"));
            staffMembers.addAll(DataAccess.getData().getUserGroup("Secretary"));
            staffMembers.addAll(DataAccess.getData().getUserGroup("Admin"));
            
            viewRemoveAccount.setStaffComboBox(staffMembers);
            
            
        }
    
    private void setComboBoxToDoctors(){
        List<String> staffMembers = new ArrayList<String>();
        
        staffMembers.addAll(DataAccess.getData().getUserGroup("Doctor"));
        
        viewGiveFeedback.setDoctorComboBox(staffMembers);
    }
    
    /** AdminController(Controller controller, Boolean accountPresent)
     *
     * Constructor for AdminController, initializes all relevant variables, sets up 
     * applicable forms, sets initial view to the default admin view
     * 
     * @param controller the super controller which contains the current controller strategy
     * @param accountPresent checks to see if there are accounts present, and switches startup policy upon this basis
     * 
     */
    public AdminController(Controller controller, Boolean accountPresent) {
        super(controller);
        
        //Initializes the specific admin forms
        viewCreateAccount = new AdminCreateAccountForm();
        viewRemoveAccount = new AdminRemoveAccountForm();
        viewReviews = new AdminViewReviewsForm();
        viewGiveFeedback = new AdminGiveFeedbackForm();
        
        //initializes the generic form listneners
        listenerCreateAccount = new CreateAccountListener();
        listenerGiveFeedback = new GiveFeedbackListener();
        listenerRemoveAccount  = new RemoveAccountListener();
        listenerViewReviews = new ViewReviewsListener();
        listenerLogout = new LogoutListener();
        
        //initializes the finalize account listener for use in the createAccount form
        listenerFinalize = new FinalizeAccountListener();
        
        //initializes the listeners for deleteAccount form
        listenerDelete = new DeleteAccountListener();
        listenerComboStaffChange = new ComboStaffBoxListener(); 
        
        //initializes the sendMessageListener for use in the giveFeedbackForm
        listenerSendMessage = new SendMessageListener();
        
        //initializes the lsiteners for the viewReviews form
        listenerComboDoctorChange = new ComboBoxDoctorListener();
        listenerComboDateChange = new ComboBoxDateListener();
        
        
        //initializes an object containing the majority of the admin functionalite
        tools = new AdminFunc();
        
        //
        view = new AdminForm();
        view.setVisible(true);
        
        
        this.accountPresent = accountPresent;
        
        //enables/disables the guis based upon accountPresent
        view.forceCreateAdmin(accountPresent);
        
        //adds listeners to the generic form
        view.addGiveFeedbackListener(listenerGiveFeedback);
        view.addRemoveAccountListener(listenerRemoveAccount);
        view.addCreateAccountListener(listenerCreateAccount);
        view.addViewDoctorReviews(listenerViewReviews);
        view.addLogoutListener(listenerLogout);
        
        //adds listeners to the createAccount form
        viewCreateAccount.addRemoveAccountListener(listenerRemoveAccount);
            viewCreateAccount.addLogoutListener(listenerLogout);
            viewCreateAccount.addViewDoctorReviews(listenerViewReviews);
            viewCreateAccount.addGiveFeedbackListener(listenerGiveFeedback);
            viewCreateAccount.addFinalizeAccountListener(listenerFinalize);
        
        //adds relevant listeners to the removeAccount form    
        viewRemoveAccount.addGiveFeedbackListener(listenerGiveFeedback);
            viewRemoveAccount.addLogoutListener(listenerLogout);
            viewRemoveAccount.addViewDoctorReviews(listenerViewReviews);
            viewRemoveAccount.addCreateAccountListener(listenerCreateAccount);
            viewRemoveAccount.addComboBoxActionListener(listenerComboStaffChange);
            viewRemoveAccount.addDeleteButtonActionListener(listenerDelete);
        
        //adds relevant listeners to the viewReviews form    
        viewReviews.addCreateAccountListener(listenerCreateAccount);
            viewReviews.addLogoutListener(listenerLogout);
            viewReviews.addRemoveAccountListener(listenerRemoveAccount);
            viewReviews.addGiveFeedbackListener(listenerGiveFeedback);
            viewReviews.addComboDateListener(listenerComboDateChange);
            viewReviews.addComboDoctorListener(listenerComboDoctorChange);
        
        //adds relevant listeners to the giveFeedback form  
        viewGiveFeedback.addCreateAccountListener(listenerCreateAccount);
            viewGiveFeedback.addLogoutListener(listenerLogout);
            viewGiveFeedback.addRemoveAccountListener(listenerRemoveAccount);
            viewGiveFeedback.addViewDoctorReviews(listenerViewReviews);
            viewGiveFeedback.addSendMessageListener(listenerSendMessage);
        
        
        
    }

    /* LogoutListener
    *
    *   ActionListener that handles logging out
    */
    class LogoutListener implements ActionListener{

        public LogoutListener() {
        }

        
        /** actionPerformed(ActionEvent e)
         *
         * Logs out the currentUser from, sets the controller strategy to the InitialController,
         * disposes of the currently used forms
         * 
         * @param ActionEvent e the super controller which contains the current controller strategy
         * 
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            getController().setStrategy(new InitialController(getController()));
            
            view.setVisible(false);
            viewGiveFeedback.setVisible(false);
            viewRemoveAccount.setVisible(false);
            viewCreateAccount.setVisible(false);
            viewReviews.setVisible(false);
            view.dispose();
            
            viewGiveFeedback.dispose();
            viewRemoveAccount.dispose();
            viewCreateAccount.dispose();
            viewReviews.dispose();
            
        }
    }
    
    /* ComboBoxDoctorListener
    *
    *   ActionListener that handles selection in ComboBoxDoctorListener
    */
    class ComboBoxDoctorListener implements ActionListener{

        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles selection in ComboBoxDoctorListener.
        *   Sets relevant view elements according to the current contents of DoctorComboBox
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            List<String> reviewDates = new ArrayList();
            
            String checker = viewReviews.getDoctorComboBox().toString();
            
            if (!"null".equals(checker)){
            
                List<Review> reviews = DataAccess.getData().findDoctor(viewReviews.getDoctorComboBox()).getReviews().getReviews();

                for (Review review:reviews ){

                    String formattedDate = review.getReviewDate().toString();

                    reviewDates.add(formattedDate);
                }
            
            }
            viewReviews.setDateTimeComboBox(reviewDates);
           
        }
        
    }
    
    /* ComboBoxDateListener
    *
    *   ActionListener that handles selection in ComboBoxDateListener
    */
    class ComboBoxDateListener implements ActionListener{

        /* ComboBoxDoctorListener
        *
        *   ActionListener that handles selection in ComboBoxDoctorListener
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String checker = viewReviews.getDoctorComboBox().toString();
            
            String feedback = ""; 
            
            String rating = "";
            
            String author = "";
            
            if (!"null".equals(checker)){

                String doctorID = DataAccess.getData().findDoctor(viewReviews.getDoctorComboBox()).getLogin().getUserName();

                String dateID = viewReviews.getDateComboBox();

                if (dateID.equals(null) == false){

                    feedback = DataAccess.getData().findDoctor(doctorID).getReviews().findReview(dateID).getFeedback();

                    rating = "Rating : " + DataAccess.getData().findDoctor(doctorID).getReviews().findReview(dateID).getRating()+ " % ";

                    author = "Author : " + DataAccess.getData().findDoctor(doctorID).getReviews().findReview(dateID).getAuthor();

                }
            
            }
            
            viewReviews.setReview(feedback);
            
            viewReviews.setRating(rating);
            
            viewReviews.setAuthor(author);
            
        }
        
    }
    
    
    
    class ComboStaffBoxListener implements ActionListener{

        
        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles selection in ComboStaffBox. sets the relevant fields, displaying the name of the selected user
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String nameOfAccountHolder;
            
            try{
                nameOfAccountHolder = 
                    "Name : "+
                    DataAccess.getData().findUser(viewRemoveAccount.getStaffComboContents()).getGivenName()+
                    " " +
                    DataAccess.getData().findUser(viewRemoveAccount.getStaffComboContents()).getSurname()
                    ;
            } catch (NullPointerException E){
                nameOfAccountHolder = "Name Unavailable";
            }
            
            viewRemoveAccount.setlblNameText(nameOfAccountHolder);
            
        }
        
    }
    
    class FinalizeAccountListener implements ActionListener{


        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles account creation. takes input from the relevant input fields and uses them to create
        *   an account with those details
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            switch (viewCreateAccount.getStaffInput()){
                case "Admin":
                    
                    tools.createAdminAccount(viewCreateAccount.getPasswordInput(),
                    viewCreateAccount.getGivenNameInput(),
                    viewCreateAccount.getSurnameInput(), 
                    viewCreateAccount.getAddressInput());
                    
                    accountPresent = true;
                    
                    viewCreateAccount.forceCreateAdmin(accountPresent);
                    
                    
                    break;
                    
                case "Doctor":
                    
                    tools.createDoctorAccount(viewCreateAccount.getPasswordInput(),
                    viewCreateAccount.getGivenNameInput(),
                    viewCreateAccount.getSurnameInput(), 
                    viewCreateAccount.getAddressInput());
                    break;
                    
                case "Secretary":
                    tools.createSecretaryAccount(viewCreateAccount.getPasswordInput(),
                    viewCreateAccount.getGivenNameInput(),
                    viewCreateAccount.getSurnameInput(), 
                    viewCreateAccount.getAddressInput());
                    break;
            
            }
            
            LoginDetails loginDetails = 
                    DataAccess.
                    getData().
                    getUsers().
                    get(DataAccess.
                            getData().
                            getUsers().
                            size()-1).
                    getLogin();
            
            JOptionPane.showMessageDialog(new JFrame(),"Account created, Username: " +  loginDetails.getUserName() + " Password: " + loginDetails.getPassWord());
            
        }
        

        
    }
    
    class DeleteAccountListener implements ActionListener{
        
        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles Account deletion. takes the relevant accountId and removes it using an instance of AdminFunc
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String userToDelete = viewRemoveAccount.getStaffComboContents();
            
            tools.removeAccount(userToDelete);
            
            JOptionPane.showMessageDialog(new JFrame(),"Account : " + userToDelete + " was deleted.") ;
            
            SetComboBoxToStaff();
            
        }
        
    }
    
    class SendMessageListener implements ActionListener{

        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles giving feedback to a chosen doctor. Takes the doctorId from the relevant ComboBox, 
        *   and text from the feedback box, and sends it via AdminFunc
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            tools.giveFeedback(viewGiveFeedback.getDoctorComboContent(), viewGiveFeedback.getMessage());
            
            JOptionPane.showMessageDialog(new JFrame(),"Message Sent.") ;
            
            viewGiveFeedback.clearMessageBox();
            
            
        }
        
    }
    
    class RemoveAccountListener implements ActionListener{

        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles setting up the RemoveAccountForm
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.setVisible(false);
            viewCreateAccount.setVisible(false);
            viewGiveFeedback.setVisible(false);
            viewReviews.setVisible(false);
            
            List<String> staffMembers = new ArrayList<String>();
                    
            staffMembers.addAll(DataAccess.getData().getUserGroup("Doctor"));
            
            staffMembers.addAll(DataAccess.getData().getUserGroup("Secretary"));
            staffMembers.addAll(DataAccess.getData().getUserGroup("Admin"));
            
            viewRemoveAccount.setStaffComboBox(staffMembers);
            
            
            
            viewRemoveAccount.setVisible(true);
            
            
            
        }
        
    }
    
    class CreateAccountListener implements ActionListener{

        public CreateAccountListener() {
        }

        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles setting up the CreateAccountForm
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
                view.setVisible(false);
                viewRemoveAccount.setVisible(false);
                viewGiveFeedback.setVisible(false);
                viewReviews.setVisible(false);
                
                viewCreateAccount.forceCreateAdmin(accountPresent);
                viewCreateAccount.setVisible(true);

        }
        
    }
    
    class ViewReviewsListener implements ActionListener {


        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles setting up the viewReviews Form
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.setVisible(false);
            viewCreateAccount.setVisible(false);
            viewRemoveAccount.setVisible(false);
            viewGiveFeedback.setVisible(false);
            
            
            List<String> staffMembers = new ArrayList<String>();
        
            staffMembers.addAll(DataAccess.getData().getUserGroup("Doctor"));
            
            
            viewReviews.setDoctorComboBox(staffMembers);
            
            viewReviews.setVisible(true);
            
            
        }

        
    }
    
    class GiveFeedbackListener implements ActionListener{

        /* actionPerformed(ActionListener e)
        *
        *   ActionListener that handles settting up the GiveFeedbackForm
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            
            view.setVisible(false);
            viewCreateAccount.setVisible(false);
            viewRemoveAccount.setVisible(false);
            viewReviews.setVisible(false);
            
            setComboBoxToDoctors();
            
            viewGiveFeedback.setVisible(true);
            
        }
        
    }
    
    
    
   
    
}
