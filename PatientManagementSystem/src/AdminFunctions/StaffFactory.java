/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminFunctions;

import UserPackage.*;

/**
 *
 * @author jedwards11
 */
public class StaffFactory {
    
    /**createAccount(Staff staffType,
            String password,
            int userNo,
            String givenName,
            String surName,
            String address
            ){
     * 
     * returns a User of type specified by the staffType Parameter.
     * said User will be created with the constructor specified within User
     *
     * @param staffType the type of staff, specified by the enum Staff. This dictates what letter prefixes the user ID.
     * @param password the password for the new account
     * @param userNo the numeric user ID used 
     * @param givenName the first name of the user of the new account
     * @param surName the surname of the user of the new account
     * @param address the address of the user of the new account
     * @return A user constructed according to the input parameters
     */
    public User createAccount(Staff staffType,
            String password,
            int userNo,
            String givenName,
            String surName,
            String address
            ){

        String id = String.format("%04d", userNo);
        
        User newUser;
        
        switch (staffType){
            case DOCTOR:
                
                id = "D" + id;
                
                newUser = new DoctorDetails(id, password, givenName, surName, address);
                
                break;
                
            case SECRETARY:
                
                id = "S" + id;
                
                newUser = new SecretaryDetails(id, password, givenName, surName, address);
                
                break;
                
            case ADMIN:
                
                id = "A" + id;
                
                newUser = new AdminDetails(id, password, givenName, surName, address);
                
                break;
            default:
                
                newUser = null;
        }
        
        return newUser;
    }
    
}
