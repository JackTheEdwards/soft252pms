/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;

import HospitalAdmin.DataAccess;
import MedicinePackage.Prescription;
import UserPackage.User;

/**
 *
 * @author User
 */
public class RequestMedicine extends Request{

    Prescription requestedPrescription;
    String recipient;
    String response;
    
    /**RequestMedicine()
     *
     * constructor method for RequestMedicine
     * 
     * @param author the author of the request
     * @param message the message of the request
     * @param requestedPrescription the requested prescription 
     */
    public RequestMedicine(User author, String message, Prescription requestedPrescription) {
        super(author, message);
        
        
        recipient = author.getLogin().getUserName();
        this.requestedPrescription = requestedPrescription;
        
    }

    /**requestTask()
     *
     * gets a response from medicine cabinet, and gives the medicine away if stocked
     * 
     */
    @Override
    protected void requestTask() {
        
        response = DataAccess.getData().getMedCab().giveMedicine(requestedPrescription, recipient);
        
    }

    /**sendAcceptResponse()
     * 
     * sends the response from medicine cabinet
     *
     * @param requestRecipient the recipient of the prescription
     */
    @Override
    protected void sendAcceptResponse(User requestRecipient) {
        
        Notification acceptMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                response
        );
        
        DataAccess.getData().findPatient(recipient).addNotification(acceptMessage);
        
        DataAccess.getData().getSecretaryRequests().remove(this);
    }

    /**sendDenyResponse(0
     * 
     * sends a deny response
     *
     * @param requestRecipient the recipient of the prescription
     */
    @Override
    protected void sendDenyResponse(User requestRecipient) {
        
        Notification denyMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                "Your request to pick up your prescription was denied"
        );
        
        
        DataAccess.getData().findPatient(recipient).addNotification(denyMessage);
        
        DataAccess.getData().getSecretaryRequests().remove(this);
        
    }
    
}
