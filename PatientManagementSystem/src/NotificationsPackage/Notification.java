/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;
import UserPackage.*;
import java.io.Serializable;
import java.util.List;
/**
 *
 * @author jedwards11
 */
public abstract class Notification implements Serializable{
    
    private User author;
    private String message;
    
    public String getMessage() {
        return message;
    }

    public Notification(User author, String message) {
        this.author = author;
        this.message = message;
    }

    User getAuthor() {
        return author;
    }
    
    
    
}
