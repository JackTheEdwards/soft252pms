/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;

import HospitalAdmin.DataAccess;
import MedicinePackage.Medicine;
import UserPackage.User;

/**
 *
 * @author User
 */
public class RequestStockMedicine extends Request{

    
    private Medicine medsToStock;
    
    /**RequestStockMedicine()
     * 
     * constructor method for RequestStockMedicine
     *
     * @param author the author of the request
     * @param message the message in the request
     * @param medsToStock the medicine to stock
     */
    public RequestStockMedicine(User author, String message, Medicine medsToStock) {
        super(author, message);
        this.medsToStock = medsToStock;
    }

    /**requestTask()
     * 
     *Stocks the medicine requested
     */
    @Override
    protected void requestTask() {
        
        DataAccess.getData().getMedCab().stockMedicine(medsToStock.getName(), medsToStock.getQuantity());
        
    }

    /**sendAcceptResponse()
     *
     * sends a message notifying the original author that request for stocking medicine was approved
     * 
     * @param currentUser the user to send the response
     */
    @Override
    protected void sendAcceptResponse(User currentUser) {
        
        
        Notification acceptMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                "Your request to stock " +
                medsToStock.getQuantity()+
                " of "+
                medsToStock.getName()+
                " has been accepted."
        );
        
        DataAccess.getData()
                .findUser(getAuthor()
                        .getLogin()
                            .getUserName())
                .addNotification(acceptMessage);
                        
        DataAccess.getData().getSecretaryRequests().remove(this);
        
    }

    /**sendDenyResponse()
     *
     * sends a message notifying the original author that request for stocking medicine was approved
     * 
     * @param currentUser the user to send the response
     */
    @Override
    protected void sendDenyResponse(User currentUser) {
        
        
        Notification denyMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                "Your request to stock " +
                medsToStock.getQuantity()+
                " of "+
                medsToStock.getName()+
                " has been denied."
        );
        
        DataAccess.getData()
                .findUser(
                        getAuthor().
                        getLogin().
                            getUserName())
                .addNotification(denyMessage);
                        
        DataAccess.getData().getSecretaryRequests().remove(this);
        
    }
    
}
