/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;

import UserPackage.User;

/**
 *
 * @author jedwards11
 */
public class Message extends Notification{

    public Message(User author, String message) {
        super(author, message);
    }
    
}
