/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;
import UserPackage.*;
import java.io.Serializable;

/**
 *
 * @author jedwards11
 */
public abstract class Request extends Notification implements Serializable{

    /**Request()
     * 
     * constructor Method for Request
     *
     * @param author the author of the request
     * @param message the message to send with the request
     */
    public Request(User author, String message) {
        super(author, message);
    }
    
    protected abstract void requestTask();
    protected abstract void sendAcceptResponse(User requestRecipient);
    protected abstract void sendDenyResponse(User requestRecipient);
    
    /**respondRequest()
     *
     * template method for handling requests
     * 
     * @param requestStatus whether the request should be accepted or denied
     */
    public void respondRequest(Boolean requestStatus){
        
        if (requestStatus){
            
            requestTask();
            
            sendAcceptResponse(this.getAuthor());
            
        }else{
            
            sendDenyResponse(this.getAuthor());
            
        }
        
        
        
    }
    
}
