/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;

import HospitalAdmin.DataAccess;
import UserPackage.PatientDetails;
import UserPackage.User;

/**
 *
 * @author User
 */
public class RequestTerminateAccount extends Request{

    /**Constructor for RequestTerminateAccount
     *
     * @param author the author of the request
     * @param message the message sent with the request
     */
    public RequestTerminateAccount(User author, String message) {
        super(author, message);
        DataAccess.getData().getSecretaryRequests().add(this);
    }

    /**requestTask()
     * 
     *removes the account specified by the request
     */
    @Override
    protected void requestTask() {
        
        PatientDetails accountToTerminate = DataAccess.getData().findPatient(getAuthor().getSurname());
        
        DataAccess.getData().getUsers().remove(accountToTerminate);
        
    }

    /**sendAcceptResponse()
     * 
     * deletes the current request from the request banks
     *
     * @param currentUser the user responding to the request
     */
    @Override
    protected void sendAcceptResponse(User currentUser) {
        
        DataAccess.getData().getSecretaryRequests().remove(this);

    }

    /**sendDenyResponse()
     * 
     * sends a message denying the request
     *
     * @param currentUser the user responding to the request
     */
    @Override
    protected void sendDenyResponse(User currentUser) {
        
        Notification denyMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                "Your request to terminate your account has been denied."
        );
        
        DataAccess.getData()
                .findPatient(getAuthor()
                        .getLogin()
                            .getUserName())
                .addNotification(denyMessage);
                        
        DataAccess.getData().getSecretaryRequests().remove(this);
    }
    
}
