/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;


import AppointmentPackage.Appointment;
import AppointmentPackage.Schedule;
import HospitalAdmin.DataAccess;
import UserPackage.User;

/**
 *
 * @author User
 */
public class RequestAppointment extends Request{

    String doctorID;
    Appointment proposedApptmt;
    String reply;
    
    /**RequestAppointment()
     *
     * Constructor method for RequestAppointment
     * 
     * @param author author of the request
     * @param message message sent with request
     * @param proposedApptmt the proposed Apoointment
     * @param doctorID doctor specified for the appointment
     */
    public RequestAppointment(User author, String message, Appointment proposedApptmt, String doctorID) {
        super(author, message);
        this.doctorID = doctorID;
        this.proposedApptmt = proposedApptmt;
    }

    /**requestTask()
     * 
     *  Attempts to book an appointment with the specified time and doctor, stores the result of this within reply,
     * and books an appointment if available
     */
    @Override
    protected void requestTask() {
        
        Schedule docSchedule = DataAccess.getData().findDoctor(doctorID).getDocSchedule();
        
        System.out.println(proposedApptmt.toString());
        
        boolean success = docSchedule.bookAppointment(proposedApptmt);
        
        if (success){
            reply = "Appointment booked for "+
                    proposedApptmt.getPatient()+
                    " with Dr "+
                    DataAccess.getData().findDoctor(doctorID).getGivenName()+
                    " "+
                    DataAccess.getData().findDoctor(doctorID).getSurname()+
                    " at "+
                    proposedApptmt.getAppointmentTime();
        }else{
            reply = "Appointment time is currently unavailable, please try again with different time or date";
        }
        
    }

    /**sendAcceptResponse()
     *
     * sends the reply gathered in requestTask()
     * 
     * @param currentUser the user to send the response
     */
    @Override
    protected void sendAcceptResponse(User currentUser) {
        
        Notification message = new Message(DataAccess.getData().getCurrentUser(), reply);
        
        DataAccess.getData().findDoctor(doctorID).addNotification(message);
        
        DataAccess.getData().findPatient(proposedApptmt.getPatient()).addNotification(message);
        
        DataAccess.getData().getSecretaryRequests().remove(this);
        
    }

    /**sendDenyResponse()
     *
     * sends a denial response
     * 
     * @param currentUser the user to send the response
     */
    @Override
    protected void sendDenyResponse(User currentUser) {
        
        Notification message = new Message(DataAccess.getData().getCurrentUser(), "Your request for an appointment was denied");
        
        DataAccess.getData().findUser(getAuthor().getLogin().getUserName()).addNotification(message);
        
        DataAccess.getData().getSecretaryRequests().remove(this);
    }
    
}
