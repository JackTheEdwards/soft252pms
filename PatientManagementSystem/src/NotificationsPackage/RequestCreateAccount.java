/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NotificationsPackage;

import HospitalAdmin.DataAccess;
import UserPackage.User;

/**
 *
 * @author User
 */
public class RequestCreateAccount extends Request{

    /**RequestCreateAccount()
     * 
     * constructor method for Request
     *
     * @param author
     * @param message
     */
    public RequestCreateAccount(User author, String message) {
        super(author, message);
    }

    /**requestTask()
     * 
     * Enables the specified account
     *
     */
    @Override
    protected void requestTask() {
        
        DataAccess.getData().findPatient(getAuthor().getLogin().getUserName()).setStatus(true);
        
    }

    /**sendAcceptResponse()
     * 
     * sends a message with the approval message
     *
     * @param currentUser the user responding to the request
     */
    @Override
    protected void sendAcceptResponse(User currentUser) {
        
        
        Notification acceptMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                "Your account has been successfully created and approved."
        );
        
        DataAccess.getData()
                .findPatient(getAuthor()
                        .getLogin()
                            .getUserName())
                .addNotification(acceptMessage);
                        
        DataAccess.getData().getSecretaryRequests().remove(this);
        
        
    }

    /**sendDenyResponse
     *
     * sends deny response
     * 
     * @param currentUser the user responding to the request
     */
    @Override
    protected void sendDenyResponse(User currentUser) {
        
        Notification acceptMessage = new Message(
                DataAccess.getData().getCurrentUser(),
                "Your request to create an account has been denied"
        );
        
        DataAccess.getData()
                .findPatient(getAuthor()
                        .getLogin()
                            .getUserName())
                .addNotification(acceptMessage);
                        
        DataAccess.getData().getSecretaryRequests().remove(this);
        
    }
    
}
