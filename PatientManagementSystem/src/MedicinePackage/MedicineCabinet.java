/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MedicinePackage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author jedwards11
 */
public class MedicineCabinet implements Serializable{
    
    private List<Medicine> medStorage = new ArrayList<Medicine>();
    
    /**stockMedicine(String medicineToStock, int amountToStock)
     *
     * Checks to see if there is currently a medicine that matches the name given by the parameter. 
     * If so, adds the amountToStock to the current quantity of that medicine, If not, add a medicine 
     * with the given name, and give it the quantity given by the amountToStock parameter
     * 
     * @param medicineToStock the name of the medicine to be stocked
     * 
     * @param amountToStock 
     */
    public void stockMedicine(String medicineToStock, int amountToStock){
        
        boolean containsMedicine = false;
        
        for (int i = 0; i < medStorage.size(); i++){
            if (medStorage.get(i).getName() == medicineToStock){
                
                containsMedicine = true;
                
                medStorage.get(i).setQuantity(medStorage.get(i).getQuantity() + amountToStock);
            }
        }
        if (!containsMedicine){
            
            Medicine medicineToAdd = new Medicine(medicineToStock, amountToStock);
            
            medStorage.add(medicineToAdd);
        
        }
        
    }
    
    /**getMedicineNames()
     *
     * returns a list of strings of the current medicines in medStorage
     * 
     * @return the list of strings in medStorage
     */
    public List<String> getMedicineNames (){
        
        List<String> medicineNameList = new ArrayList<String>();
        
        for (Medicine medName: medStorage){
            medicineNameList.add(medName.getName());
        }
        return medicineNameList;
    }
    
    /**giveMedicine (Prescription prescription, String recipient)
     *
     * Looks at the medicine provided in the prescription, and takes that amount of medicine from medStorage, returning 
     * a string specifying how much of which medicine is being given to the recipient provided.
     * If there is no medicine with that name currently in medstorage, or not enough of that medicine to give, then
     * a string is returned saying "Medicine not currently in Stock, Please Try again later"
     * 
     * @param prescription The prescription of the recipient, from which the type and amount of medicine can be derived
     * 
     * @param recipient The name of the person who has the prescription
     * 
     * @return a string specifying the amount of the medicine given to the recipient, or a message
     * saying that the medicine is current unavailable, and to try again later
     */
    public String giveMedicine (Prescription prescription, String recipient){

        String returnString = "Medicine not currently in Stock, Please Try again later";
        
        Medicine medsToGet = prescription.getMeds();
        int amountOfMedsToGet = prescription.getMeds().getQuantity();
        
        for (int i = 0; i < medStorage.size(); i++){
            if (medStorage.get(i).getName() == medsToGet.getName() ){
                
                if (medStorage.get(i).getQuantity()  >   amountOfMedsToGet){
                    
                    medStorage.get(i).setQuantity(medStorage.get(i).getQuantity() - amountOfMedsToGet);
                    
                    returnString = "Giving " + amountOfMedsToGet + " of " + medsToGet.getName() + " to " + recipient;
                    
                    return returnString;
                     
                }
            }
        }
        
        return returnString;
        
        
    }

    /**getMedStorage()
     *
     * accessor method for medStorage field
     * 
     * @return the list of medicine in medStorage
     */
    public List<Medicine> getMedStorage() {
        return medStorage;
    }

    
}
