/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MedicinePackage;

import java.io.Serializable;

/**
 *
 * @author jedwards11
 */
public class Medicine implements Serializable{
    private String name;
    private int quantity;

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        
        if (quantity >=0){
            this.quantity = quantity;
        }else {
            this.quantity = 0;
        }
    }


    public Medicine(String name, int quantity) {
        this.name = name;
        
        if (quantity >=0){
            this.quantity = quantity;
        }else {
            this.quantity = 0;
        }
    }
    
    
    
}
