/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MedicinePackage;

import java.io.Serializable;

/**
 *
 * @author jedwards11
 */
public class Prescription implements Serializable{
    
    private Medicine meds;
    
    private String instructions;

    public Prescription(Medicine meds, String instructions) {
        this.meds = meds;
        this.instructions = instructions;
    }

    public Medicine getMeds() {
        return meds;
    }

    public String getInstructions() {
        return instructions;
    }
    
    
    
    
    
}
