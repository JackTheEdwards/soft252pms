/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserPackage;
import LoginPackage.*;
import NotificationsPackage.Notification;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author jedwards11
 */
public abstract class User implements Serializable{
    
    private LoginDetails login;
    private String givenName;
    private String surname;
    private String address;
    private List<Notification> receivedNotifications;

    public User(String userName, String password, String givenName, String surname, String address) {
        
        this.login = new LoginDetails(userName, password);
        this.address = address;
        this.givenName = givenName;
        this.surname = surname;
        receivedNotifications = new ArrayList<Notification>();
    }

    public String getAddress() {
        return address;
    }

    
    
    public LoginDetails getLogin() {
        return login;
    }

    public void setLogin(LoginDetails login) {
        this.login = login;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public void addNotification(Notification notification){
        receivedNotifications.add(notification);
        
    }
    
    public void removeNotification(int notificationIDToRemove){
        receivedNotifications.remove(notificationIDToRemove);
    }

    public List<Notification> getReceivedNotifications() {
        return receivedNotifications;
    }
    
    
    
    
    
    
}
