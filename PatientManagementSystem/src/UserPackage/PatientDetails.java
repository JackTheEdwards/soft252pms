/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserPackage;
import MedicinePackage.Prescription;
import PatientFunctions.PatientBuilder;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author jedwards11
 */
public class PatientDetails extends User implements Serializable{


    private Sex sex;
    private Timestamp dateOfBirth;
    private List<String> patientNotes = new ArrayList<String>();
    private List<Prescription> prescriptions = new ArrayList<Prescription>();
    private Boolean status;

    public List<String> getPatientNotes() {
        return patientNotes;
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public Sex getSex() {
        return sex;
    }

    public Timestamp getDateOfBirth() {
        return dateOfBirth;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    
    
    //DEPRECATED, USE PatientBuilder CLASS INSTEAD
    public PatientDetails(Sex sex, Timestamp dateOfBirth, String userName, String password, String givenName, String surname, String address) {
        super(userName, password, givenName, surname, address);
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        status = false;
    }
    
    public PatientDetails(PatientBuilder builder){
        
        super(builder.getLogin().getUserName(),
                builder.getLogin().getPassWord(),
                builder.getGivenName(),
                builder.getSurname(),
                builder.getAddress());
        
        this.sex = builder.getSex();
        this.dateOfBirth = builder.getDateOfBirth();
        status = false;
    }
    
    
    
    
}
