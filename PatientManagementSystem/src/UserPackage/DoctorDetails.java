/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserPackage;
import AppointmentPackage.Schedule;
import RatingsPackage.*;
import java.io.Serializable;
/**
 *
 * @author jedwards11
 */
public class DoctorDetails extends User implements Serializable{
    
    private RatingsCollection reviews;
    
    private Schedule docSchedule;

    public RatingsCollection getReviews() {
        return reviews;
    }

    public DoctorDetails(String userName, String password, String givenName, String surname, String address) {
        
        
        super(userName, password, givenName, surname, address);
        
        reviews = new RatingsCollection ();
        docSchedule = new Schedule();
        
        
    }

    public Schedule getDocSchedule() {
        return docSchedule;
    }


    
}
