/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppointmentPackage;

import UserPackage.DoctorDetails;
import UserPackage.PatientDetails;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author jedwards11
 */
public class Appointment implements Serializable{
    
    private String patientID;
    private Timestamp appointmentTime;

    /** getPatient()
     *
     * Accessor method
     * 
     * @return the ID of the patient
     */
    public String getPatient() {
        return patientID;
    }

    /**getAppointmentTime()
     *
     * Accessor method
     * 
     * @return the timestamp of the appointment
     */
    public Timestamp getAppointmentTime() {
        return appointmentTime;
    }

    /**Appointment()
     * 
     * Constructor method
     *
     * @param patientID the ID of the patient
     * @param appointmentTime the timestamp of the appointment
     */
    public Appointment(String patientID, Timestamp appointmentTime) {
        this.patientID = patientID;
        this.appointmentTime = appointmentTime;
    }
    
    
}
