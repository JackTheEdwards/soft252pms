/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppointmentPackage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.time.Instant;

/**
 *
 * @author jedwards11
 */
public class Schedule implements Serializable{
    
    private List<Appointment> appointments;

    /**
     *
     * Accessor method
     * 
     * @return
     */
    public List<Appointment> getAppointments() {
        return appointments;
    }

    /**
     *
     * Constructor method
     * 
     */
    public Schedule() {
        this.appointments = new ArrayList<Appointment>();
    }
    
    /**bookAppointment()
     * 
     * takes an appointment as an input, and checks to see if that appointment slot is available.
     * if it is, adds it to the list of booked appointments, and returns true. if not, returns false
     *
     * @param proposedAppt the appointment to book
     * @return whether the appointment was successfully booked
     */
    public Boolean bookAppointment (Appointment proposedAppt){
        
        Timestamp proposedApptStart = proposedAppt.getAppointmentTime();
        
        Instant duration = proposedApptStart.toInstant().plusSeconds(2700);
        
        Timestamp proposedApptEnd  = Timestamp.from(duration);
        
        for (Appointment currentAppointment : getAppointments()){
            
            Timestamp currentApptStart = currentAppointment.getAppointmentTime();
            
            duration = currentApptStart.toInstant().plusSeconds(2700);
            
            Timestamp currentApptEnd = Timestamp.from(duration);
            
            if (
                    (
                    proposedApptStart.before(currentApptEnd) && proposedApptStart.after(currentApptStart))||(
                    proposedApptStart.equals(currentApptStart))||(
                    proposedApptEnd.before(currentApptEnd) && proposedApptEnd.after(currentApptStart))
                    ){
                return false;
            }
            
        }
        
        appointments.add(proposedAppt);
        
        return true;
        
    }
    
    
}
