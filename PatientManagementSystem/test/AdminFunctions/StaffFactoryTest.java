/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminFunctions;

import UserPackage.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jedwards11
 */
public class StaffFactoryTest {
    
    StaffFactory staffMaker;
    
    //String arrays corresponding to the details of the user to be created.
    //Array value 0 is the given name, value 1 is the surname, 2 is their password, 3 is the address
    
    
    String[] userOne = {"Jimmy", "Jones", "password1234", "Ebrington Street"};
    
    public StaffFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        staffMaker = new StaffFactory();
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreateAccount() {
        
        String expectedUsername = "S0001";
        String expectedPassword = "password1234";
        String expectedFirstName = "Jimmy";
        String expectedSurname = "Jones";
        String expectedAddress = "Ebrington Street";
        
        User newUser = staffMaker.createAccount(Staff.SECRETARY, userOne[2], 1, userOne[0], userOne[1], userOne[3]);
        String actualUsername = newUser.getLogin().getUserName();
        assertEquals("Testing username is formatted correctly when userNo is in single digits and staffType is Secretary", expectedUsername, actualUsername);
        
        expectedUsername = "A0034";
        newUser = staffMaker.createAccount(Staff.ADMIN, userOne[2], 34, userOne[0], userOne[1], userOne[3]);
        actualUsername = newUser.getLogin().getUserName();
        assertEquals("Testing username is formatted correctly when userNo is in double digits and staffType is Admin", expectedUsername, actualUsername);
        
        expectedUsername = "D5079";
        newUser = staffMaker.createAccount(Staff.DOCTOR, userOne[2], 5079, userOne[0], userOne[1], userOne[3]);
        actualUsername = newUser.getLogin().getUserName();
        assertEquals("Testing username is formatted correctly when userNo is in quad digits and staffType is Doctor", expectedUsername, actualUsername);
        
        assertEquals("Testing password is initialized correctly", expectedPassword, newUser.getLogin().getPassWord());
        assertEquals("Testing First Name is initialized correctly", expectedFirstName, newUser.getGivenName());
        assertEquals("Testing surname is initialized correctly", expectedSurname, newUser.getSurname());
        assertEquals("Testing address is initialized correctly", expectedAddress, newUser.getAddress());
        
        
    }
    
}
