/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppointmentPackage;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jedwards11
 */
public class ScheduleTest {
    
    Schedule testSchedule;
    
    public ScheduleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        testSchedule = new Schedule();
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAppointments() {
    }

    @Test
    public void testBookAppointment() {
        
        LocalDateTime date = LocalDateTime.of(2020, Month.MARCH, 15, 16, 30);
        Timestamp apptDate = Timestamp.valueOf(date);
        String patientID = "Dave";
        Appointment initialAppt = new Appointment(patientID, apptDate);
        
        date = LocalDateTime.of(2020, Month.MARCH, 15, 16, 30);
        apptDate = Timestamp.valueOf(date);
        Appointment sameAppt = new Appointment (patientID, apptDate);

        date = LocalDateTime.of(2020, Month.MARCH, 15, 16, 00);
        apptDate = Timestamp.valueOf(date);
        Appointment tooEarlyAppt = new Appointment (patientID, apptDate);
        
        date = LocalDateTime.of(2020, Month.MARCH, 15, 16, 45);
        apptDate = Timestamp.valueOf(date);
        Appointment tooLateAppt = new Appointment (patientID, apptDate);
        
        date = LocalDateTime.of(2020, Month.MARCH, 15, 17, 30);
        apptDate = Timestamp.valueOf(date);
        Appointment subsequentAppt = new Appointment (patientID, apptDate);
        
        assertTrue("Checking that bookAppointment returns true, indicating the appointment is booked sucessfully", testSchedule.bookAppointment(initialAppt));
        
        assertTrue("Checking that the appointment has been added to the schedule ", testSchedule.getAppointments().contains(initialAppt));
        
        assertFalse("Checking that an appointment cannot be booked at the same time as an existing appointment", testSchedule.bookAppointment(sameAppt));
        assertFalse("checking that the early appointment has not been added to the schedule", testSchedule.getAppointments().contains(sameAppt));
        
        assertFalse("Checking that an early appointment cannot be booked at the same time as an existing appointment", testSchedule.bookAppointment(tooEarlyAppt));
        assertFalse("checking that the early appointment has not been added to the schedule", testSchedule.getAppointments().contains(tooEarlyAppt));
        
        assertFalse("Checking that a later appointment cannot be booked at the same time as an existing appointment", testSchedule.bookAppointment(tooLateAppt));
        assertFalse("checking that the late appointment has not been added to the schedule", testSchedule.getAppointments().contains(tooLateAppt));
        
        assertTrue("Checking that a appointment can be booked afterwards", testSchedule.bookAppointment(subsequentAppt));
        assertTrue("Checking that the subsequent appointment has been added to the schedule ", testSchedule.getAppointments().contains(subsequentAppt));
        
    }
    
}
