/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoginPackage;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jedwards11
 */
public class LoginControllerTest {
    
    private LoginDetails JimmyJones;
    private LoginDetails Susan251;
    private LoginDetails sgtpepper;
    private LoginDetails[] TeamRoster = new LoginDetails[4];
    
    private LoginController doorMan = new LoginController();
    
    /**
     *
     */
    public LoginControllerTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @After
    public void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        
        
        
        JimmyJones = new LoginDetails("Jimmy1234", "password");
        
        Susan251 = new LoginDetails("Susan251", "1234");
        
        sgtpepper = new LoginDetails("sgtpepper", "fishbowl");
        
        TeamRoster[0] = new LoginDetails("Admin","6ge3f7dqds#");
        TeamRoster[1] = new LoginDetails("Xx_Edgelord_x","kittyCat");
        TeamRoster[2] = new LoginDetails("Susan251", "12345");
        TeamRoster[3] = new LoginDetails("sgtpepper", "fishbowl");
        
        
        
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of checkUser method, of class LoginController.
     */
    @Test
    public void testCheckUser() {
        String expResult ;
        String actualResult ;

        expResult = "Invalid Username";
        actualResult = doorMan.checkUser(JimmyJones, TeamRoster);
        
        assertEquals("Checking attempting to login as JimmyJones fails due to invalid Username",expResult, actualResult);
        
        expResult = "Invalid Password";
        actualResult = doorMan.checkUser(Susan251, TeamRoster);
        
        assertEquals("Checking attempting to login as Susan251 fails due to invalid password",expResult, actualResult);
        
        expResult = "Login Successful";
        actualResult = doorMan.checkUser(sgtpepper, TeamRoster);
        
        assertEquals("Checking attempting to login as sgtpepper is successful",expResult, actualResult);
        
    }
    
    //Tested private methods, uncomment and set access to be tested as protected if further modification is needed 

//    /**
//     * Test of checkUserName method, of class LoginController.
//     */
//    @Test
//    public void testCheckUserName() {
//        System.out.println("Testing checkUsername()");
//        
//        
//        assertTrue("Checking Susan is in Roster",doorMan.checkUserName(Susan251.getUserName(), TeamRoster));
//        
//        assertTrue("Checking sgtpepper is in Roster",doorMan.checkUserName(sgtpepper.getUserName(),TeamRoster));
//        
//        assertFalse("Checking JimmyJones is not in Roster",doorMan.checkUserName(JimmyJones.getUserName(), TeamRoster));
//        
//        
//        
//    }
//
//    /**
//     * Test of checkPassWord method, of class LoginController.
//     */
//    @Test
//    public void testCheckPassWord() {
//
//        assertTrue("Checking sgtpepper has correct password",doorMan.checkPassWord(sgtpepper, TeamRoster));
//        
//        assertFalse("Checking Susan251 has incorrect password",doorMan.checkPassWord(Susan251, TeamRoster));
//        
//    }
//
    /**
     * Test of findUser method, of class LoginController.
     */
    @Test
    public void testFindUser() {
        int expResult = 3;
        
        int actualResult = doorMan.findUser("sgtpepper", TeamRoster);
        
        assertEquals("checking sgtpepper is index 3 ", expResult, actualResult);
        
    }
    
    
    
}
