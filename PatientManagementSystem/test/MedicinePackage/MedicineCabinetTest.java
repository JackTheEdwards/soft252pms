/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MedicinePackage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jedwards11
 */
public class MedicineCabinetTest {
    
    MedicineCabinet medCab;
    
    Medicine Cream;
    
    Medicine CuppaTea;
    
    Medicine Gaffa;
    
    Medicine sonicDrugs;
            
    Prescription goFast;
    
    Prescription brokenLegs;
    
    public MedicineCabinetTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        medCab = new MedicineCabinet();
        
        Cream = new Medicine("FaceCream", 0);
        
        CuppaTea = new Medicine("Nice cup of earl grey", 123);
        
        Gaffa = new Medicine("Duct Tape", 43);
        
        sonicDrugs = new Medicine("Speedphetamines", 5);
        
        goFast = new Prescription(sonicDrugs, "Take 5 a day with coffee to go fast");
        
        brokenLegs = new Prescription(Gaffa, "Apply liberally to damaged leg");
        
        
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of stockMedicine method, of class MedicineCabinet.
     */
    @Test
    public void testStockMedicine() {
        
        int expValue = 0;
        
        assertTrue ("Checking that FaceCream is not in the medicine cabinet", medCab.getMedStorage().isEmpty());
        
        System.out.println(medCab.toString());
        
        medCab.stockMedicine(Cream.getName(), Cream.getQuantity());
        
        System.out.println(medCab.toString());
        
        assertEquals ("Checking that FaceCream is now in the medicine cabinet", medCab.getMedStorage().get(0).getName(), Cream.getName() );
        
        assertEquals ("Checking that FaceCream quantity is 0", medCab.getMedStorage().get(0).getQuantity(), expValue);
        
        expValue = 5;
        
        medCab.stockMedicine(Cream.getName(), 5);
        
        assertEquals ("Checking that FaceCream quantity is 5", medCab.getMedStorage().get(0).getQuantity(), expValue);
        
        expValue = 10;
        
        medCab.stockMedicine(Cream.getName(), 5);
        
        assertEquals ("Checking that FaceCream quantity is 10", medCab.getMedStorage().get(0).getQuantity(), expValue);
        
        
    }

    /**
     * Test of giveMedicine method, of class MedicineCabinet.
     */
    @Test
    public void testGiveMedicine() {
        
        String expResult = "Medicine not currently in Stock, Please Try again later";
        
        assertTrue ("Checking that the medicine cabinet is empty", medCab.getMedStorage().isEmpty());
        
        assertEquals("Checking that medicineCabinet returns"
                + " Not in stock message when that medicine has"
                + " never been added to medStorage",
                expResult,
                medCab.giveMedicine(goFast, "HedgeHog")
        );
        
        medCab.stockMedicine(sonicDrugs.getName(), 0);
        
        assertEquals("Checking that medicineCabinet returns"
                + " Not in stock message when that medicine has"
                + " been added to medStorage, but has a quantity of 0",
                expResult, 
                medCab.giveMedicine(goFast, "HedgeHog")
        );
        
        medCab.stockMedicine(sonicDrugs.getName(), 8);
        
        expResult = "Giving " + sonicDrugs.getQuantity() + " of " + sonicDrugs.getName() + " to HedgeHog";
        
        assertEquals("Checking that medicineCabinet returns \"Giving "
                + "\" + sonicDrugs.getQuantity() + \" of \" + sonicDrugs.getName()"
                + " + \" to HedgeHog\", when there is an adequate quantity of medicine"
                + "in the Medicine Cabinet",
                expResult, 
                medCab.giveMedicine(goFast, "HedgeHog")
        );
        
        expResult = "Medicine not currently in Stock, Please Try again later";
        
        assertEquals("Checking that medicineCabinet returns"
                + " Not in stock message when there was an adequate"
                + "quantity of medicine but that quantity has been "
                + "diminished and is no longer adequate",
                expResult, 
                medCab.giveMedicine(goFast, "HedgeHog")
        );
        
       
    }
    
}
