/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RatingsPackage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jedwards11
 */
public class RatingsCollectionTest {
    
    Review Troll;  
    Review Critic;
    Review Fanboy;
    Review Depressive;
    
    RatingsCollection reviews = new RatingsCollection();
    
    public RatingsCollectionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        Troll = new Review("needs more peanut butter on the butt","XX_SWAGMASTER_XX",20);
        Critic = new Review("Not quite up to the serious potential promised by this range of sofas","Richard E Lemon",62);
        Fanboy = new Review("MARK RUFFALO COULD SNAP A KITTENS NECK ON ME MUM'S GRAVE AND I'D BE COOL WITH IT","MCUFANBOY",99);
        Depressive = new Review ("My life is mashed potato", "SoSad8734", 45);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAverageRating method, of class RatingsCollection.
     */
    @Test
    public void testGetAverageRating() {
//        String expectedResult = "No reviews available";
//        
//        assertEquals("Checking the method returns no reviews available if no reviews are provided", expectedResult, reviews.getAverageRating());


        fail("Test not implemented");
    }

    /**
     * Test of addReview method, of class RatingsCollection.
     */
    @Test
    public void testAddReview() {
        assertFalse("Checking Critic is not in the list of reviews", reviews.getReviews().contains(Critic));
        
        reviews.addReview(Critic);
        
        assertTrue("Checking Critic was added to the list of reviews", reviews.getReviews().contains(Critic));
        
    }

    /**
     * Test of removeReview method, of class RatingsCollection.
     */
    @Test
    public void testRemoveReview() {
        reviews.addReview(Troll);
        
        assertTrue("Checking Troll was added to the list of reviews", reviews.getReviews().contains(Troll));
        
        reviews.removeReview(Troll);
        
        assertFalse("Checking Troll was removed from the list of reviews", reviews.getReviews().contains(Troll));
        
        
    }

    

    /**
     * Test of getReviews method, of class RatingsCollection.
     */
    @Test
    public void testGetReviews() {
        fail("Test not implemented (Generated code, lower priority)");
    }

    /**
     * Test of findReview method, of class RatingsCollection.
     */
    @Test
    public void testFindReview() {
        fail("Test not implemented");
    }
    
    
    /**
     * Test of CalculateRatingTest method, of class RatingsCollection.
     */
    @Test
    public void testCalculateRatingTest() {
        int expResult = 62;
        
        reviews.addReview(Critic);
        
        assertEquals("1 review, expected result = 62", expResult, reviews.CalculateRatingTest(),0.5);
        
        reviews.addReview(Troll);
        
        expResult = 41;
        assertEquals("2 review, expected result = 41", expResult, reviews.CalculateRatingTest(),0.5);
        
        reviews.addReview(Fanboy);
        
        expResult = 60;
        assertEquals("3 review, expected result = 60", expResult, reviews.CalculateRatingTest(),0.5);
        
        reviews.addReview(Depressive);
        
        expResult = 57;
        assertEquals("4 review, expected result = 57", expResult, reviews.CalculateRatingTest(),0.5);
        
        

        
    }
    
}
